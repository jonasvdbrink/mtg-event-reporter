from mtgreporter import Tournament

t = Tournament()

with open('players.txt', 'r') as infile:
    for player in infile:
        t.add_player(player)

# t.create_random_pairings()

torgeir = t.get_player('Torgeir')
emil = t.get_player('Emil')
tbear = t.get_player('Thorbjorn')
lasse = t.get_player('Lasse')
gm = t.get_player('Geir Magne')
nh = t.get_player('Nils Haakon')
victor = t.get_player('Victor')
paal = t.get_player('Paal')
emil = t.get_player('Emil')
marius = t.get_player('Marius')
torgeir = t.get_player('Torgeir')
jonas = t.get_player('Jonas')
jon = t.get_player('Jon Magnus')


# Round 1
emil.register_match(torgeir, 2, 0, 0)
gm.register_match(lasse, 2, 1, 0)
jonas.register_match(victor, 2, 1, 0)
nh.register_match(tbear, 2, 1, 0)
paal.register_match(jon, 2, 0, 0)
marius.give_bye()

t.print_standings()

# # Round 2
# marius.register_match(gm, 2, 0, 0)
# emil.register_match(paal, 2, 1, 0)
# nh.register_match(jonas, 2, 0, 0)
# lasse.register_match(victor, 2, 1, 0)
# tbear.register_match(jon, 2, 1, 0)
# torgeir.give_bye()


# # Round 3
# marius.register_match(nh, 0, 1, 1)
# emil.register_match(tbear, 2, 0, 0)
# paal.register_match(lasse, 0, 2, 0)
# torgeir.register_match(gm, 2, 0, 0)
# jonas.register_match(jon, 1, 2, 0)
# victor.give_bye()

# # Round 4
# emil.register_match(nh, 1, 1, 1)
# torgeir.register_match(lasse, 0, 2, 0)
# marius.register_match(tbear, 0, 2, 0)
# gm.register_match(victor, 1, 2, 0)
# jonas.register_match(paal, 0, 2, 0)
# jon.give_bye()

# # Round 5
# emil.register_match(lasse, 0, 0, 3)
# nh.register_match(torgeir, 0, 2, 0)
# tbear.register_match(paal, 0, 2, 0)
# marius.register_match(victor, 2, 1, 1)
# jon.register_match(gm, 1, 2, 0)
# jonas.give_bye()

# t.print_standings()

# print lasse.compare_score(emil)

