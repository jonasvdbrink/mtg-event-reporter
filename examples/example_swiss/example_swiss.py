from mtgreporter import Tournament

# Create Tournament object
t = Tournament(seed=None, start_table=121)

# Add players to tournament from seperate file
t.add_players_from_file('players.txt')

# Print nr of players or list players by name
#print(t.nr_players())
#t.print_players()

# Write seatings for player meeting
#t.write_player_meeting()

# Create pairings for Round 1
# t.create_pairings()
# t.write_pairings_by_name()
# t.write_pairings_by_table()

# Round 1
t.register_match('Gideon', 'Nissa', 2, 0)
t.register_match('Bolas', 'Chandra', 2, 1)
t.register_match('Ugin', 'Liliana', 2, 1)
t.register_match('Elspeth', 'Venser', 2, 1)
t.register_match('Garruk', 'Koth', 2, 0)
t.register_bye('Sorin')
t.register_loss('Jace')

# Write Standings after round 1
t.write_standings()

# Create pairings for Round 2
# t.create_pairings()
# t.write_pairings_by_name()
# t.write_pairings_by_table()

# Round 2
# t.register_match('Sorin', 'Bolas', 2, 0)
# t.register_match('Gideon', 'Garruk', 2, 1)
# t.register_match('Elspeth', 'Ugin', 2, 0)
# t.register_match('Chandra', 'Liliana', 2, 1)
# t.register_match('Venser', 'Koth', 2, 1)
# t.give_bye('Nissa')

# Write Standings after round 2
# t.write_standings()


# Create pairings for Round 3
# t.create_pairings()
# t.write_pairings_by_name()
# t.write_pairings_by_table()

# Round 3
t.register_match("Sorin", "Elspeth", 0, 1, 1)
t.register_match("Gideon", "Venser", 2, 0)
t.register_match("Garruk", "Chandra", 0, 2)
t.register_match("Nissa", "Bolas", 2, 0)
t.register_match("Ugin", "Koth", 1, 2)
t.give_bye("Liliana")

# Write Standings after round 3
#t.write_standings()

# Drop Players
#t.drop("Koth")
#t.drop("Venser")

# Create pairings for Round 4
# t.create_pairings()

# Round 4
t.register_match("Gideon", "Elspeth", 2, 1)
t.register_match("Sorin", "Chandra", 1, 1, 1)
t.register_match("Nissa", "Garruk", 0, 2)
t.register_match("Bolas", "Liliana", 1, 2)
# t.write_pairings_by_name()
# t.write_pairings_by_table()

     
# Write Final standings
# t.print_standings(cutoff=4)
