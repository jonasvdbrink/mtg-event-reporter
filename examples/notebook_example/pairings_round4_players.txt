Swiss Tournament 18.03.2017

Pairings for Round 4

   Table   Player 1     vs. Player 2    
----------------------------------------------------------------
     124   Bolas (3)    vs. Liliana (3) 
     122   Chandra (6)  vs. Sorin (6)   
     121   Elspeth (9)  vs. Gideon (9)  
     123   Garruk (3)   vs. Nissa (6)   
     121   Gideon (9)   vs. Elspeth (9) 
     124   Liliana (3)  vs. Bolas (3)   
     123   Nissa (6)    vs. Garruk (3)  
     122   Sorin (6)    vs. Chandra (6) 
       -   Ugin (3)         BYE         
----------------------------------------------------------------
