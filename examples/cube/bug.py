import sys
import numpy as np
sys.path.append('../..')

from mtgreporter.tournament import Tournament

# Create Tournament object
t = Tournament(seed=None, start_table=121)

# Add players to tournament from seperate file
t.add_players_from_file('players.txt')

#t.print_fillin()

### MATCH RESULTS FOR ROUND 1
t.register_match('Finn-Erik', 'Gøran', 0, 2)
t.register_match('GM', 'Jonna', 1, 2)
t.register_match('Jonas', 'Torgeir', 2, 0)
t.register_match('Marius', 'Sindre', 2, 1)
t.register_bye('Madland')

### MATCH RESULTS FOR ROUND 2
t.register_match('Madland', 'Jonna', 0, 2)
t.register_match('Finn-Erik', 'GM', 1, 2)
t.register_match('Jonas', 'Marius', 1, 2)
t.register_match('Gøran', 'Torgeir', 2, 1)
t.register_bye('Sindre')

t.drop_player('Torgeir')

t.create_random_pairings()
t.print_pairings_by_table()