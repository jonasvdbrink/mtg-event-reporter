# -*- encoding: utf-8 -*-

import unittest
import sys
sys.path.append('../src/')

from Player import Player
from PlayerList import PlayerList, \
                       TournamentIsFull, PlayerNotInTournament, \
                       PlayerAlreadyInTournament

class TestPlayerListMethods(unittest.TestCase):
    def test_add(self):
        p1 = Player("Alice")
        p2 = Player(u"Båbb")
        playerlist = PlayerList()

        playerlist.add(p1)
        playerlist.add(p2)
        playerlist.add("Christine")
        playerlist.add(u"Kåre")
        self.assertRaises(PlayerAlreadyInTournament, playerlist.add, p1)
        
        

    def test_is_enrolled(self):
        p1 = Player("Alice")
        p2 = Player("Bob")
        playerlist = PlayerList()
        self.assertFalse(playerlist.is_enrolled(p1))
        playerlist.add(p1)
        self.assertTrue(playerlist.is_enrolled(p1))
        self.assertFalse(playerlist.is_enrolled(p2))

    def test_get(self):
        p1 = Player("Alice")
        playerlist = PlayerList()
        playerlist.add(p1)
        self.assertEqual(playerlist.get("Alice"), p1)
        self.assertRaises(PlayerNotInTournament, playerlist.get, "Bob")

    def test_remove(self):
        p1 = Player("Alice")
        p2 = Player("Bob")
        playerlist = PlayerList()
        playerlist.add(p1)
        playerlist.add(p2)
        self.assertTrue(playerlist.is_enrolled(p1))
        self.assertTrue(playerlist.is_enrolled(p2))
        playerlist.remove(p1)
        self.assertFalse(playerlist.is_enrolled(p1))
        self.assertTrue(playerlist.is_enrolled(p2))
        self.assertRaises(PlayerNotInTournament, playerlist.remove, p1)
        self.assertRaises(PlayerNotInTournament, playerlist.remove, "Carol")

    def test_get_current_matches(self):
        p1 = Player("Alice")
        p2 = Player("Bob")
        playerlist = PlayerList()
        playerlist.add(p1)
        playerlist.add(p2)
        p1.match_with(p2)
        self.assertEqual(playerlist.get_current_matches(), [p1.last_match])
        self.assertEqual(playerlist.get_current_matches(), [p2.last_match])

    def test_get_unreported_matches(self):
        p1 = Player("Alice")
        p2 = Player("Bob")
        playerlist = PlayerList()
        playerlist.add(p1)
        playerlist.add(p2)
        p1.match_with(p2)
        self.assertEqual(playerlist.get_unreported_matches(), [p1.last_match])
        self.assertEqual(playerlist.get_completed_matches(), [])
        p1.register_match(p2, 2, 1)
        self.assertEqual(playerlist.get_unreported_matches(), [])
        self.assertEqual(playerlist.get_completed_matches(), [p1.last_match])

    def test_get_active_players(self):
        p1 = Player("Alice")
        p2 = Player("Bob")
        p3 = Player("Carol")
        p4 = Player("Dylan")
        playerlist = PlayerList()
        for p in p1, p2, p3, p4:
            playerlist.add(p)
        activeplayers = playerlist.get_active_players()
        activeplayers.sort()
        self.assertEqual(activeplayers, [p1, p2, p3, p4])
        p2.drop(0)
        activeplayers = playerlist.get_active_players()
        activeplayers.sort()
        self.assertEqual(activeplayers, [p1, p3, p4])

    def test_get_players_by_name(self):
        p1 = Player("Alice")
        p2 = Player("Bob")
        p3 = Player("Carol")
        p4 = Player("Dylan")
        playerlist = PlayerList()
        for p in p1, p2, p3, p4:
            playerlist.add(p)
        self.assertEqual(playerlist.get_players_by_name(), [p1, p2, p3, p4])

    def test_get_players_by_standing(self):
        p1 = Player("Alice")
        p2 = Player("Bob")
        p3 = Player("Carol")
        p4 = Player("Dylan")
        playerlist = PlayerList()
        for p in p1, p2, p3, p4:
            playerlist.add(p)
        p1.match_with(p2)
        p3.match_with(p4)
        p1.register_match(p2, 2, 1)
        p3.register_match(p4, 2, 0)
        self.assertEqual(playerlist.get_players_by_standing(), [p3, p1, p2, p4])

if __name__ == '__main__':
    unittest.main()

