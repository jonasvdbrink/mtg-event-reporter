API
===

..  automodule:: mtgreporter.player
    :members: Player, ByePlayer, PlayerList
    