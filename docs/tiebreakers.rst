How Tiebreakers are Calculated
==============================

Tiebreakers in MTG Event Reporter are implemented as described in the `DCI document on Tiebreakers <https://www.wizards.com/dci/downloads/tiebreakers.pdf>`_. 

Players are ranked based on match points (MP). If two or more players are even in match points, the following tiebreakers are used in order

1. Compare all tied contenders based on their opponents’ match-win percentages (OMW).
2. Compare all tied contenders based on their game-win percentages (GWP).
3. Compare all tied contenders based on their opponents’ game-win percentages (OGW).

If two or more players are exactly tied on both match points and all three tie breakers, the result should be considered a tie–if a winner must be chosen (for example for a cut-off to top 8), a winner should be selected randomly.

**Note:** The DCI tiebreaker document specifies that MWP is lower bound at 0.33, but that GWP is *not*. However, it states that GWP should be considered lower bound when calculating OGW. This is a discrepancy in the document, and it is not clear that it is supposed to work like this. In our code we have implemented both MWP and GWP to be lower bound at 0.33. (*Update:* The MTR states that GWP is also supposed to be lower bound at 0.33, making it clear that the DCI document is erroneous.)

OMW and OGW are weighted equally for all opponents, even if those opponents drop from the tournament. If a player recieves a Bye in the first round, they will not have a defined OMW/OGW as they have met 0 opponents thus far.




