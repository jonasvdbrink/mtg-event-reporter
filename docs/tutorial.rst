===============
Getting Started
===============

Holding a tournament using the MTG Event Reporter is simply done by creating
and continuously editing a python script. (There are plans to have a functional
GUI in the finalized version of MER, but it does not currently have one).

I recommend creating a fresh folder/directory for your tournament, and create
your script inside that folder, that way, all the output files for printing
standings/pairings to paper are kept in a structured manner and is easier
to look up later.

Note that mtgreporter should be run in Python 3 to have proper Unicode support.

As you run your tournament, you will update your script to print standings, 
create pairings, report match results and so forth. Don't worry, this tutorial 
will take you through everything you need to hold a successful tournament.

After creating your empty script, the first thing you will need to do is import
what you will need to hold a tournament, which is the ``Tournament`` class. 

.. code-block:: python
    
    from mtgreporter import Tournament

You can now create an object of this class. MER is object oriented, and the 
tournament object will store all information about your tournament, and provide
practical methods for moving your tournament along. The constructor takes some 
optional meta-data, such as for example the name of the tournament or a starting
table number, or a custom number of rounds

.. code-block:: python

    tournament = Tournament(name='Vintage 15-proxy 30.04.17', start_table=121, nr_rounds=6)

This meta-data is fully optional, and for smaller tournaments, such as a kitchen
table draft, you can just skip them. If no name is given, the name defaults to 
``Swiss Tournament dd.mm.yy``, using the current date. Likewise, if the number 
of rounds are not given, it will default to the standard number of rounds 
for a swiss tournament, i.e., the base-2 logarithm of the number of players, 
rounded up.

Another keyword that is accepted by the constructor is a seed, which is used by the
RNG to create the pairings. Note that even if you do not set a seed, it defaults to a
specific seed. This is done so that rerunning the same input gives the same output. 
If you want to run MER without a specific seed, just pass in ``seed=None`` to the
 constructor.

A tournament object will always start without any players in it, so the next
step will be to add your players.

===================
Registering Players
===================

In MER, players are identified by their names, so be careful that no two players
entering into the tournament have identical names. If two players happen to have
identical names, add a discriminatory postfix to separate them, or add playful
nicknames. Also, no player is allowed to have the name BYE, as this is reserved
for the system.

There are essentially three ways to add players

1. You can do it one by one

.. code-block:: python

    tournament.add_player("Gideon Jura")
    tournament.add_player("Nissa Revane")
    tournament.add_player("Chandra Nalaar")

2. ...or you can add a sequence of players all at once

.. code-block:: python

    tournament.add_players(["Gideon Jura", "Nissa Revane", "Chandra Nalaar"])

3. ...or you can read players from a text file, each line of the text file is 
interpreted as a single player name
5
.. code-block:: python

    tournament.add_players_from_file("players.txt")

For tournaments with 10+ players, the last method is recommended. I just let the players 
write themselves into a text-file at registration, and then load up the file to quickly
add all players to the tournament, circumventing bad had writing at the same time!

Once players are added to the tournament, you might want to check that all players are 
actually enrolled before proceeding to make sure everything is as it should. The method
``tournament.print_players()`` prints the number of players registered and then lists
them by name. If you just want to see the number of registered players, you can 
do ``print(tournament.nr_players())``. The method ``tournament.get_players()`` and
similar return a list of ``Player`` objects, and is only useful for advanced use 
cases.

Once you have checked that all players seem to be registered to the tournament, you
might want to hold a player meeting. To help seat players alphabetically by name, there
are two helpful methods:

1. You can print the info to the terminal with the method

.. code-block:: python

    tournament.print_player_meeting()

2. Or you can write it to a text-file

.. code-block:: python

    tournament.write_player_meeting()

You can supply a custom filename to save to by using the ``filename`` keyword argument, 
if you do not supply one it defaults to ``playermeeting.txt``. The system default text editor 
will also be prompted to open the file, and from there you can print the list to paper.

**Note:** MER treats names a single string, so when players are sorted alphabetically, such as for the player meeting, it will normally be by first name, not by last name. It might be a good idea to let your judges be aware of this, as it will impact how decklists are ordered, etc.

After registering all players, and holding a player meeting, you should get ready to post pairings for the first round, so let us look at how pairings can be created and printed.

=================
Dropping Players
=================

If you want to drop a player from the tournament, simply use the method

.. code-block:: python
    
    tournament.drop_player("Nissa Revane")

Make sure you have reported the results of the last match the player played before dropping them, as this will impact tie breaker calculations of the dropped player's opponents.

=================
Creating Pairings
=================

Creating pairings is done simply by calling a method with no arguments

.. code-block:: python

    tournament.create_pairings()

Note that pairings can only be created if all players who are active, i.e., who have not been dropped, have played the same number of rounds. This is to make sure that one does not forget to register results, or register two matches to a player by accident.

The ``create_pairings()`` method will automatically judge whether it is the last round of the Swiss (which changes pairing behavior), the number of rounds for your swiss can be given to the ``Tournament`` constructor as described above, or it will default to the number of rounds listed in: :doc:`pairings`. However, you can override this behavior with the keyword ``last_round=False`` or ``last_round=True``. An alternative if you want to be more explicit and just ignore the number of rounds metadata is to instead call the methods

    - ``tournament.create_random_pairings()``
    - ``tournament.create_greedy_pairings()``

Where 'random' is used for the initial rounds, and 'greedy' the final round.

Once the pairings are created, you will want to list them. As before, there is both a `print` method to print directly to the terminal, and a `write` which writes it to a text file. The first is most likely the most practical for smaller tournaments were you read pairings out loud, while the latter is useful if the pairings are to be printed to paper. In addition you can print pairing in order of table number, or in order of names.

.. code-block:: python

    # Print to terminal
    tournament.print_pairings_by_table()
    tournament.print_pairings_by_name()
    tournament.print_pairings() # defaults to 'by_table'
    tournament.print_pairings(order='name')
    
    # Write to 'pairings_{round_nr}.txt'
    tournament.write_pairings_by_table() 
    tournament.write_pairings_by_name()
    tournament.write_pairings()
    tournament.write_pairings(order='name') 

    # Write to custom file by table number
    tournament.write_pairings(filename='vintage_round3.txt')

**Note:** These are meant to be called after pairings are created, but none of the results are reported. If you print/write pairings after a few results are reported, the write-out might look weird or an error might be thrown. This will be improved in the future.
    
===================
Registering Results
===================

To register results to your tournament, you will have to call a `register_match()` method on your tournament object, to make this a bit simpler, there is a `print_fillin()` method which will print out all the necessary line of codes to report the matches for the current round. From the terminal, you can then copy paste these lines into your tournament script and fill inn the match results.

.. code-block:: python

    tournament.print_fillin()

Whether you use the print fillin method to save some time, or write in the code manually, you register matches by calling a method on the tournament object:

.. code-block:: python

    tournament.register_match('Nissa Revane', 'Chandra Nalaar', 1, 2)

Here, you first give the two players, identified by their name, and then the match results. The first number is the number of games won by the first player, and the second number is the number of games won by the second player. If there are any drawn games, you can write those as a third number. This means an intentional draw should be registered as:

.. code-block:: python

    tournament.register_match('Nissa Revane', 'Chandra Nalaar', 0, 0, 3)

or you could use the alias method

.. code-block:: python

    tournament.register_id('Nissa Revane', 'Chandra Nalaar')

To register a bye you could either enter the name `Bye` for the second player, or simply use an alias

- ``tournament.register_match('Nissa Revane', 'Bye', 2, 0, 0)``
- ``tournament.register_bye('Nissa Revane')``

Alternatively, you can register matches directly on the Player objects, if you have them stored

- ``nissa.register_match(chandra, 2, 0)``
- ``gideon.register_draw(jace)``
- ``karn.register_bye()``

There is no functional difference if you register the match through the `Tournament` or the `Player` object. In either case the match is appended to the players match history, which will be reflected in the standings and future pairings.

==================
Printing Standings
==================

Just like for the pairings, the standings can either be printed to the terminal, or written to a file, this can be done through the commands

- ``tournament.print_standings()``
- ``tournament.write_standings()``

Both of these methods takes the optional `cutoff` keyword, which will impose a dashed line to indicate the cutoff to a top 4 or top 8, for example ``tournament.write_standings(cutoff=8)``.

As before, the write method takes an optional filename, if no name is given it defaults to:

- ``standings_<tournament-name>_<round_number>.txt``

The flag ``dropped`` can be set to true if one wants to include dropped players in the standings, but this is off by default.


