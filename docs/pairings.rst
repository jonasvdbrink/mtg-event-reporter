How Pairings are Found
======================

Currently, MTG Event Reporter only supports pairing following the Swiss Tournament Style outlined by DCI. See the `DCI Guide to Swiss Pairings <http://www.wizards.com/dci/downloads/swiss_pairings.pdf/>`_ if you are unfamiliar with this system. 

The pairing rules in MTG Event Reporter has been modelled to follow the DCI document. However, the document does not explain edge cases in particular detail, and therfore edge case behaviour can be a bit different from similar reporting software such as WER.

The following conditions are used when pairing players:

1. No two players should meet more than once during the swiss portion of a tournament.
2. No player shall recieve more than one bye during the swiss portion of a tournament.
3. A bye shall be given to a player among those with the least points among those who have yet to recieve a bye.
4. As far as possible, people should be paired with players with the same amount of match points, if players with different match points must be paired, the point difference should be minimized.

In some cases, all conditions cannot be fulfilled simultaneously. In such cases, they should break in order of last to first, i.e., we would rather players with different points meet, than a player recieving two byes and so forth.

In order to implement the pairing system into an algorithm, we produce a *player pairing graph*. This is a mathematical graph where the active players in the tournament are represented as the nodes. The graph is complete, with the weight of the vertex between two players representing the 'cost' of pairing those two players. We define the vertices with negative weights (-∞, 0] and use a weighted maximum matching graph algorithm that produces a set of pairings that minimizes this cost. If all conditions can be upheld, the cost will resultingly be minimized to 0. The implementation of the weighted maximum matching graph algorithm is taken from `Joris VR <http://jorisvr.nl/article/maximum-matching>`_, where you can also read more about how the algorithm works.

For the last round of a swiss tournament, it is common to use greedy, i.e., non-random pairing. This is not explicitly stated in the Swiss Pairings document, but it is common practice. To do this, MTG Event Reporter iterates over all active players in order of their current standing. For each player, he or she is paired to the top-rated player that they have not yet met and who is yet to be paired to somewhere. Any players that are not succsesfully matched after this process is done is given a bye - this is not an optimal implementation and might lead to some wonky pairings for those ranked at the bottom of the standings.

================
Number of Rounds
================

The number of rounds in a Swiss tournament depends on the number of players and is as follows:

========  =======
Players   Rounds 
========  =======
3–4        2
5–8        3
9–16       4
17–32      5
33–64      6
65–128     7
129–212    8
213+       9+
========  =======

Above 212 players, the pattern for number of players for Swiss tournaments changes, otherwise there would be too many rounds. See DCI documentation for how this is handled at Grand Prixes and similar events.

For tournaments below 8 players, a swiss tournament is discouraged. Alternatives are a single-elimination or double-elimination bracket, or a round-robin tournament. MTG Event Reporter does not currently support these tournamt types, though it can be used in the case of round robin to compute tie-breakers.

========
Play-ofs
========

After the final round of the swiss tournament is finished, there is usually a cut-off to a top 4 or top 8 play-of based on standings. As mentioned in the tiebreaker section, if two or more players are exactly tied to enter the play-ofs, one of them should be selected randomly.

We recommend the following Play-ofs

=======  ===== 
Players  Type
=======  =====
0–8      n/a     
9–16     Top 4   
17+      Top 8   
=======  =====

The play-of tournament is a single-elimination bracket, seeded by standings. For the top 8 the pairings are done as follows

 Seed 1 plays seed 8
 Seed 2 plays seed 7
 Seed 3 plays seed 6
 Seed 4 plays seed 5

The winners of the first and last match, and the winners of the middle two matches meet in the semi-finals, and the winner of the semi-finals meet in the final match.

For a top 4 it is

 Seed 1 plays seed 4
 Seed 2 plays seed 3

And the winners meet in the finals

