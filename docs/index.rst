.. MTG Event Reporter documentation master file, created by
   sphinx-quickstart on Tue Mar  7 15:23:56 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MTG Event Reporter Tutorial
===========================

Welcome to the MTG Event Reporter, a small tool meant to help those who want to organize MTG tournaments, but who lack access to WER.

This software is only developed erraticly in my spare time, and so has not yet reached a level where I would recommend it for use in the wild. If you are interested in using the software for your own tournaments, please get in touch and I will try to speed up the development process.

.. toctree::
    :maxdepth: 2
    :caption: Tutorial

    tutorial

.. toctree::
   :maxdepth: 1
   :caption: Details

   pairings
   tiebreakers
   

