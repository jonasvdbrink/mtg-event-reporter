# -*- coding: utf-8 -*-

import os
import math
import time
import random
import webbrowser
import decimal
from .player import Player, DummyPlayer, PlayerList, PlayerAlreadyMatched
from .mwmatching import maxWeightMatching

class RoundNotFinished(Exception):
    """Thrown if pairings are created before all matches are reported."""
    pass

class RoundFinished(Exception):
    """Thrown if pairings are printed after all results are given"""
    pass


class Tournament(object):
    """Tournament superclass.
    
    The class stores all players in the tournament in a PlayerList,
    each Player object keeps track of their own match results so 
    that the current standings can be compiled. 

    Pairings for the next round can be created based on the current 
    results.
    """
    def __init__(self, start_table=1, seed=123456789, name=None, nr_rounds=None):
        """A tournament starts empty of players, but can contain metadata.

        Parameters
        ----------
        name : str, optional
            Name of the tournament, defaults to 'Swiss Tournament dd/mm/yy'
        start_table : int, optional
            What table to start counting from, defaults to 1
        seed : int or None, optional
            Select the seed for the RNG, or set to None if no seed is to be used
        """
        self._players = PlayerList()
        if seed is not None:
            random.seed(seed)
        self.start_table = start_table
        if name is not None:
            self.tournament_name = name
        else:
            self.tournament_name = "Swiss Tournament {}".format(time.strftime("%d.%m.%Y"))
        self.nr_rounds = nr_rounds

    def add_player(self, player):
        """Add a new playre to the tournament

        Parameters
        ----------
        player : Player object or string
            If the argument is a string, a new Player object with that 
            name will be created.
        """
        self._players.add(player)

    def add_players(self, players):
        """Add a sequence of players to the tournament.

        Parameters
        ----------
        players : sequence of Player objects or strings
        """
        for player in players:
            self.add_player(player)

    def add_players_from_file(self, filename):
        """Read players from a file.

        Each non-empty line of the infile will be interpreted
        as the name of a player, no metadata is required or supported.

        Parameters
        ----------
        filename : str
            path to file to read players from
        """
        with open(filename, 'r') as infile:
            for line in infile:
                self._players.add(line)

    def delete_player(self, name):
        """Delete a player from the tournament.

        A player should only be deleted during registration,
        a dropped player should **not** be deleted, only marked as
        dropped. Otherwise, this will ruin the current standings.

        TODO: Add error-handling for wrong use.

        Parameters
        ----------
        name : str
            Name of the player to drop
        """
        self._players.remove(name)

    def drop_player(self, name):
        """Drop a player from the tournament.

        A dropped player will not show up in the standings, and 
        will not be included in pairings in any further rounds,
        they are however still important for calculating standings.

        Parameters
        ----------
        name : str
            Name of the player to drop
        """
        player = self._players.get(name)
        player.drop()

    def drop(self, name):
        """Alias for drop_player"""
        self.drop_player(name)

    def round_breaker(self, breaker):
        breaker = decimal.Decimal(str(breaker))
        rounded_breaker = breaker.quantize(decimal.Decimal('0.001'), 
                                           rounding=decimal.ROUND_HALF_UP)
        return float(rounded_breaker)

    @property
    def rounds_completed(self):
        """The number of rounds played.

        The number of rounds played in a tournament is defined as
        the minmum number of matches completed across all active
        players. This means that the round number is not incremented
        before the final match result of the round is reported.
        """
        matches_played = [p.nr_matches for p in self.get_active_players()]
        return min(matches_played)

    @property
    def current_round(self):
        """The current round number of the tournament.

        The current round number is simply the number of finished rounds 
        plus one, meaning the current round is automatically incremented
        as soon as the final result of the previous round is reported.
        """
        return self.rounds_completed + 1
        
    def nr_players(self):
        return len(self._players)

    def nr_of_players(self):
        return len(self._players)

    def get_nr_players(self):
        return len(self._players)

    def get_nr_of_players(self): 
        return len(self._players)

    def get_player(self, name):
        """Get a player in the Tournament by name

        Parameters
        ----------
        name : str
            Name of the player

        Returns
        -------
        Player object
        """
        return self._players.get_player(name)

    def get_players(self, dropped=False):
        """Return list of all players enrolled in the tournament.

        Parameters
        ----------
        dropped : boolean, optional
            Decides if dropped players shold be included in returned list
        """
        if dropped:
            return self.get_all_players()
        else:
            return self.get_active_players()

    def get_active_players(self):
        """Return list of undropped players"""
        return self._players.get_active_players()

    def get_dropped_players(self):
        """Return list of dropped players"""
        return self._players.get_dropped_players()

    def get_all_players(self):
        """Return list of all players, including dropped ones."""
        return self._players.get_all_players()

    def get_players_by_name(self, dropped=False):
        """Return a list of active players sorted alphabetically"""
        return self._players.get_players_by_name(dropped)

    def get_players_by_standing(self, dropped=False):
        return self._players.get_players_by_standing(dropped)

    def get_player_names(self, dropped=False):
        """Return a list of all player names in alphabetically order."""
        return [str(p) for p in self.get_players_by_name(dropped)]

    def register_match(self, p1, p2, gw, gl, gd=0):
        """Register a result of a match between two players

        Note that the reported score is relative to the order 
        of the players, which is a common source of errors 
        when reporting results!

        Parameters
        ----------
        p1 : str
            Name of the first player
        p2 : str
            Name of the second player
        gw : int
            Number of games won by first player
        gl : int
            Number of games lost by first player
        gd : int, optional
            Number of games drawn, defaults to 0 if not given
        """
        if p2 == 'BYE':
            self.get_player(p1).register_bye()
        else:
            self.get_player(p1).register_match(self.get_player(p2), gw, gl, gd)

    def register_bye(self, name):
        """Register a Bye to the given player."""
        self.get_player(name).register_bye()

    def give_bye(self, name):
        """Alias for register_bye."""
        self.register_bye(name)

    def bye(self, name):
        """Alias for register_bye."""
        self.register_bye(name)

    def register_loss(self, name):
        """Gives the player a Match loss without pairing them against a specific player.

        Note that this function will give the player a match loss against a Dummy Player,
        this is mainly meant to be used if a player is registered for the tournament too late
        and must start with match losses.

        If a player recieves a match loss penalty during a tournament, it should be applied
        directly towards the match in question, or towards that players next match, following 
        normal pairing, meaning it should be registered as a normal match result.
        """
        self.get_player(name).register_loss()

    def give_loss(self, name):
        """Alias for register_loss."""
        self.register_loss(name)        

    def has_met(self, p1, p2):
        """Check if two players have already played each other

        Parameters
        ----------
        p1 : str
            Name of first player
        p2 : str
            Name of second player
        """
        return self.get_player(p1).has_met(self.get_player(p2))

    def print_standings(self, dropped=False, cutoff=None):
        """Print the current standings to terminal.

        Parameters
        ----------
        dropped : bool, optional
            Whether or not to include dropped players in the standings
        cutoff : int, optional
            If supplied, prints a line to signifiy a cutoff
        """
        print("Standings after Round {}".format(self.rounds_completed))
        standings = self.get_players_by_standing(dropped=True)
        fmt = '{pos:6} {name:24}{mp:<10}{omw:^5.3}   {gwp:^5.3}   {ogw:^5.3}'
        print(fmt.format(pos='Pos.', name='Name', mp='Points', 
                         omw='OMW', gwp='GWP', ogw='OGW'))
        print('-'*64)
        fmt = '{pos:>6} {name:24}{mp:<10}{omw:^5.3f}   {gwp:^5.3f}   {ogw:^5.3f}'
        for i, player in enumerate(standings, 1):
            if not player.has_dropped:
                print(fmt.format(pos=i, name=player.name,
                                 mp=player.MP, omw=self.round_breaker(player.OMW),
                                 gwp=self.round_breaker(player.GWP), ogw=self.round_breaker(player.OGW)))
            elif dropped:
                print(fmt.format(pos='D-'+str(i), name=player.name,
                                 mp=player.MP, omw=self.round_breaker(player.OMW),
                                 gwp=self.round_breaker(player.GWP), ogw=self.round_breaker(player.OGW)))

            if cutoff is not None:
                cutoff += player.has_dropped
                if i == cutoff:
                    print('   {}\n'.format('*'*60))
        print('-'*64)
        print('\n')

    def write_standings(self, filename=None, dropped=False, cutoff=None, show=True):
        """Write the current standings to file and open it.

        Parameters
        ----------
        filename : str, optional
            Path to save the standings. If none is given, it is automatically
            saved to the file 'standings_round<nr>.txt'
        dropped : bool, optional
            Whether or not to include dropped players in the standings
        cutoff : int, optional
            If supplied, prints a line to signifiy a cutoff
        show : bool, optional
            Whether to trigger a text editor to open the written file
        """
        if filename is None:
            filename = "standings_round{}.txt".format(self.rounds_completed)
        else:
            if '.' not in filename:
                filename += '.txt'

        standings = self.get_players_by_standing(dropped=True)

        with open(filename, 'w+') as outfile:
            outfile.write(self.tournament_name + "\n\n")
            outfile.write("Standings after Round {}\n\n".format(self.rounds_completed))   

            fmt = '{pos:6} {name:24}{mp:<10}{omw:^5.3}   {gwp:^5.3}   {ogw:^5.3}\n'
            outfile.write(fmt.format(pos='Pos.', name='Name', mp='Points', 
                                     omw='OMW', gwp='GWP', ogw='OGW'))
            outfile.write('-'*64 + '\n')
            fmt = '{pos:>6} {name:24}{mp:<10}{omw:^5.3f}   {gwp:^5.3f}   {ogw:^5.3f}\n'
            for i, player in enumerate(standings, 1):
                if not player.has_dropped:
                    outfile.write(fmt.format(pos=i, name=player.name,
                                             mp=player.MP, omw=self.round_breaker(player.OMW),
                                             gwp=self.round_breaker(player.GWP), ogw=self.round_breaker(player.OGW)))
                elif dropped:
                    outfile.write(fmt.format(pos='D-'+str(i), name=player.name,
                                             mp=player.MP, omw=self.round_breaker(player.OMW),
                                             gwp=self.round_breaker(player.GWP), ogw=self.round_breaker(player.OGW)))
                if cutoff is not None:
                    cutoff += player.has_dropped
                    if i == cutoff:
                        outfile.write('   {}\n'.format('*'*60))
            outfile.write('-'*64)
            outfile.write('\n')
        print("Standings for round {} written to file '{}'".format(self.rounds_completed, filename))
        # Open the newly written file
        if show:
            webbrowser.open(filename)

    def create_pairings(self, last_round=None):
        """Create pairings for next round.

        Pairings are created based on the current standings

        Parameters
        ---------- 
        last_round : bool, optional
            Flag to enable deterministic (greedy) pairings for last round
            of swiss

        Raises
        ------
        RoundNotFinished
        """
        if self.nr_rounds is None:
            self.nr_rounds = math.ceil(math.log(self.nr_players(), 2))
        if last_round is None:
            if self.current_round < self.nr_rounds:
                self.create_random_pairings()
            elif self.current_round == self.nr_rounds:
                self.create_greedy_pairings()
            else:
                raise RuntimeError('Current round ({}) > nr_rounds ({})'.format(self.current_round, self.nr_rounds))
        elif last_round is False:
            self.create_random_pairings()
        elif last_round is True:
            self.create_greedy_pairings()

    def print_pairings(self, order='table'):
        """Print the current matches to the terminal.

        Parameters
        ----------
        order : str, optional
            Must be 'table', 'player', or 'name'. Decides what order to print
            the pairings in. Defaults to table.
        """
        if order.lower() in ['table']:
            self.print_pairings_by_table()
        elif order.lower() in ['player', 'name']:
            self.print_pairings_by_name()
        else:
            raise ValueError("'order' keyword must be 'table','player' or 'name'")

    def print_pairings_by_table(self):
        """Print all matches in order of table number."""
        # Check if pairings are made
        if list(self._players.get_unreported_matches()) == []:
            raise RoundFinished("All matches for this round are finished.")
        
        print(self.tournament_name + "\n")
        print("Pairings for Round {}".format(self.current_round))
        matches = self._players.get_current_matches()  
        namewidth = max([len(m.player_one.name) for m in matches]) + 5
        fmt = '  {table:>6}   {p1:%d} vs. {p2:%d}' % (namewidth, namewidth)
        print(fmt.format(table='Table', p1='Player 1', p2='Player 2'))
        print('-'*64)
        for m in matches:
            p1 = m.player_one.name
            p2 = m.player_two.name
            t = m.table or '-'
            if p2 != 'BYE':
                print(fmt.format(table=t, 
                                 p1=p1+" ({})".format(self.get_player(p1).MP),
                                 p2=p2+" ({})".format(self.get_player(p2).MP)))
            else:
                print(fmt.format(table=t, 
                                 p1=p1+" ({})".format(self.get_player(p1).MP-3),
                                 p2='BYE').replace('vs.', '   '))
        print('-'*64)
        print("\n")

    def print_pairings_by_player(self):
        if list(self._players.get_unreported_matches()) == []:
            raise RoundFinished("All matches for this round are finished.")
        
        print(self.tournament_name + "\n")
        print("Pairings for Round {}".format(self.current_round))
        players = self.get_active_players()
        namewidth = max([len(p.name) for p in players]) + 5
        fmt = '  {table:>6}   {p1:%d} vs. {p2:%d}' % (namewidth, namewidth)
        print(fmt.format(table='Table', p1='Player 1', p2='Player 2'))
        print('-'*64)
        for p1 in players:
            match = p1.current_match
            if match:
                p2 = p1.get_opponent()
                print(fmt.format(table=match.table or '-', 
                                 p1=p1.name+" ({})".format(p1.MP),
                                 p2=p2.name+" ({})".format(p2.MP)))
            else:
                print(fmt.format(table='-', 
                                 p1=p1.name+" ({})".format(p1.MP-3),
                                 p2='BYE').replace('vs.', '   '))
        print('-'*64)
        print("\n")

    def print_pairings_by_name(self):
        """Alias for print_pairings_by_player."""
        self.print_pairings_by_player()

    def write_pairings(self, filename=None, order='table', show=True):
        if order.lower() in ['table']:
            self.write_pairings_by_table(filename=filename, show=show)
        elif order.lower() in ['player', 'name']:
            self.write_pairings_by_name(filename=filename, show=show)
        else:
            raise ValueError("'order' keyword must be 'table','player' or 'name'")

    def write_pairings_by_table(self, filename=None, show=True):
        """Write the current pairings to file and open it.

        Parameters
        ----------
        filename : str, optional
            Path to save the pairings, if none is given, it is automatically
            saved to the file 'pairings_round<nr>.txt'
        show : bool, optional
            Whether to trigger a text editor to open the written file
        """
        if filename is None:
            filename = "pairings_round{}_tables.txt".format(self.current_round)
        else:
            if '.' not in filename:
                filename += '.txt'

        matches = self._players.get_current_matches()
        if list(self._players.get_unreported_matches()) == []:
            raise RoundFinished("All matches for this round are finished.")
        
        with open(filename, 'w+') as outfile:
            outfile.write(self.tournament_name + "\n\n")
            outfile.write("Pairings for Round {}\n\n".format(self.current_round))
    
            namewidth = max([len(m.player_one.name) for m in matches]) + 5
            fmt = '  {table:>6}   {p1:%d} vs. {p2:%d}\n' % (namewidth, namewidth)
            outfile.write(fmt.format(table='Table', 
                                     p1='Player 1', 
                                     p2='Player 2'))
            outfile.write('-'*(2*namewidth+24) + '\n')
            for m in matches:
                p1 = m.player_one.name
                p2 = m.player_two.name
                t = m.table or '-'
                if p2 != 'BYE':
                    outfile.write(fmt.format(table=t, 
                                             p1=p1+" ({})".format(self.get_player(p1).MP),
                                             p2=p2+" ({})".format(self.get_player(p2).MP)))
                else:
                    outfile.write(fmt.format(table=t, 
                                             p1=p1+" ({})".format(self.get_player(p1).MP-3),
                                             p2='BYE').replace('vs.', '   '))
            outfile.write('-'*(2*namewidth + 24) + '\n\n')
        print("Pairings for round {} written to file '{}'".format(self.current_round, filename))
        # Open the newly written file
        if show:
            webbrowser.open(filename)

    def write_pairings_by_player(self, filename=None, show=True):
        if filename is None:
            filename = "pairings_round{}_players.txt".format(self.current_round)
        else:
            if '.' not in filename:
                filename += '.txt'

        if list(self._players.get_unreported_matches()) == []:
            raise RoundFinished("All matches for this round are finished.")
        
        with open(filename, 'w+') as outfile:
            outfile.write(self.tournament_name + "\n\n")
            outfile.write("Pairings for Round {}\n\n".format(self.current_round))
            players = self.get_active_players()
            namewidth = max([len(p.name) for p in players]) + 5
            fmt = '  {table:>6}   {p1:%d} vs. {p2:%d}\n' % (namewidth, namewidth)
            outfile.write(fmt.format(table='Table', p1='Player 1', p2='Player 2'))
            outfile.write('-'*64 + '\n')
            for p1 in players:
                match = p1.current_match
                if match:
                    p2 = p1.get_opponent()
                    outfile.write(fmt.format(table=match.table or '-', 
                                     p1=p1.name+" ({})".format(p1.MP),
                                     p2=p2.name+" ({})".format(p2.MP)))
                else:
                    outfile.write(fmt.format(table='-', 
                                     p1=p1.name+" ({})".format(p1.MP-3),
                                     p2='BYE').replace('vs.', '   '))
            outfile.write('-'*64)
            outfile.write("\n")
            print("Pairings for round {} written to file '{}'".format(self.current_round, filename))
            # Open the newly written file
            if show:
                webbrowser.open(filename)

    def write_pairings_by_name(self, filename=None, show=True):
        """Alias for write_pairings_by_player."""
        self.write_pairings_by_player(filename=filename, show=show)

    def print_fillin(self):
        if list(self._players.get_unreported_matches()) == []:
            raise RoundFinished("All matches for this round are finished.")
        
        matches = self._players.get_current_matches()
        print("### MATCH RESULTS FOR ROUND {}".format(self.current_round))
        
        for m in matches:
            p1 = m.player_one.name
            p2 = m.player_two.name
            t = m.table or '-'
            if p2 != 'BYE':
                print("t.register_match('{}', '{}', , )".format(p1, p2))       
            else:
                print("t.register_bye('{}')".format(p1))
                
    def print_players(self, dropped=False):
        """List all players in the tournament."""
        names = [p.name for p in self.get_players()]
        namewidth = max([len(n) for n in names])
        print("{} players registered".format(len(names)))
        print("-"*(namewidth + 2))
        print("\n".join((str(p) for p in 
                                   self._players.get_players(dropped=dropped))))
        print("-"*(namewidth + 2))

    def create_random_pairings(self):
        """Create pairings for the next round based on current results

        A max weight graph algorithm is used to find a set of pairings 
        that optimally fulfills the DCI swiss pairing rules.

        If more than one such solution exists, one is chosen at random.
        """
        players, weights = self._create_player_pairing_graph()
        opponents = maxWeightMatching(weights, maxcardinality=True)
        standings = self._players.get_players_by_standing()
        table = self.start_table

        while standings:
            # Select top player yet to be paired
            p = standings.pop(0)
            # Find their opponent
            o = players[opponents[players.index(p)]]
            # Create the match and link it to the players
            if isinstance(o, DummyPlayer):
                p.register_bye()
            else:
                p.match_with(o, table=table)
                standings.remove(o)
                table += 1

    def create_greedy_pairings(self):
        """Create pairings for the last round of the swiss

        For the last round of the swiss, pairings are not random, but 
        deterministically based on the standings.

        FIXME: Need to figure out how to do byes. This method might create 
        more than one bye, as it focueses solely on top-down pairing.
        """
        players = self.get_players_by_standing()
        table = self.start_table
        byes = 0

        while players:
            player = players.pop(0)
            for opponent in players:
                if not player.has_met(opponent):
                    players.remove(opponent)
                    player.match_with(opponent, table=table)
                    table += 1
                    break
            else:
                player.register_bye()
                byes += 1

        if byes > 1:
            print("Warning: The greedy pairing produced {} byes. You might want"
                  " to manually repairing to avoid multiple byes".format(byes))

    def print_player_meeting(self):
        """Print seatings for the player meeting to the terminal."""
        table = float(self.start_table)
        players = self.get_active_players()
        namewidth = max([len(p.name) for p in players]) + 5
        fmt = '  {table:>6}   {name:%d}' % (namewidth)
        
        print(self.tournament_name)
        print("Player Meeting. {} players registered.".format(len(players)))
        print(fmt.format(table='Table', name='Name'))
        print('-'*(namewidth + 15))
        for player in players:
            print(fmt.format(table=int(table),
                             name=player.name))
            table += 0.5
        print('-'*(namewidth + 15))
        print("\n")

    def write_player_meeting(self, filename=None, show=True):
        """Write seatings for the player meeting to a file."""
        table = float(self.start_table)
        players = self.get_active_players()
        namewidth = max([len(p.name) for p in players]) + 5
        fmt = '  {table:>6}   {name:%d}\n' % (namewidth)

        if filename is None:
            filename = "player_meeting.txt"
        else:
            if '.' not in filename:
                filename += '.txt'

        with open(filename, 'w+') as outfile:
            outfile.write(self.tournament_name + '\n')
            outfile.write("Player Meeting. {} players registered.\n\n".format(len(players)))
            outfile.write(fmt.format(table='Table', name='Name'))
            outfile.write('-'*(namewidth + 15) + '\n')
            for player in players:
                outfile.write(fmt.format(table=int(table), name=player.name))
                table += 0.5
            outfile.write('-'*(namewidth + 15) + '\n')
        print("Seatings for player meeting written to file '{}'".format(filename))
        # Open the newly written file
        if show:
            webbrowser.open(filename)


    def _create_player_pairing_graph(self):
        """Set up the player pairing graph

        The player pairing graph is a mathematical graph were the nodes 
        represent the active players of the tournament, and the edges 
        the negative costs for pairing a given pair of players. The 
        negative cost is used as we use a max weight algorithm.

        The cost is defined as the square of the difference in match 
        points between the players. In addition a high (static) cost
        is given to pairing two players who have already met. Giving
        a Bye to a player is more costly the more match points 
        the given player has, and giving a player a Bye if they already
        have one is also very costly.

        TODO: Should perform rigorous testing on some corner cases on 
        these rules.
        """
        players = self.get_active_players()
        random.shuffle(players)
        N = len(players)
        weights = []
        for i in range(N):
            for j in range(i+1, N):
                wij = -(players[i].MP - players[j].MP)**2
                wij -= 1000000*(players[i].has_met(players[j]))
                weights.append((i, j, wij))
        # Add Virtual DummyPlayer vertex if neccesary
        if N % 2 == 1:
            players.append(DummyPlayer())
            for i in range(N):
                wij = -100*players[i].MP - players[i].has_had_bye*10000
                weights.append((i, N, wij))
        return players, weights

    def _create_greedy_pairing_graph(self):
        """Set up the greedy pairing graph

        The pairings for the last round takes into account the standing of 
        the players, rather than their match points, however, we still do 
        not want the same two players to meet, or a player to be given a second
        bye.

        The cost is defined as the square of the differences in the current
        standings between the two players. In addition a high static cost 
        is given to pairing two players who have already met or giving 
        a bye to a player who has already received one.

        TODO: Should perform rigorous testing on some corner cases on 
        these rules.
        """
        players = self.get_players_by_standing()
        N = len(players)
        standings = list(range(N))
        weights = []

        for i in range(N):
            print("{:<20}".format(players[i].name), end='')
            for j in range(i+1, N):
                if players[i].has_met(players[j]):
                    wij = -10**12
                    print("{:>10s}".format('Has Met'), end='')
                else:
                    wij = -(N - standings[i]) * (N - standings[j]) * (standings[i] - standings[j])**4
                    print("{:>10d}".format(wij), end='')
                weights.append((i, j, wij))
        if N % 2 == 1:
            players.append(DummyPlayer())
            for i in range(N):
                wiN = 0
                #wiN -= players[i].has_had_bye * 10**6
                #wiN -= 100*(N - standings[i])**2
                weights.append((i, N, wij))
    
        return players, weights
