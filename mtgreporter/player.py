# -*- coding: utf-8 -*-

"""
This module includes classes for representing a players. Each player is 
represented by a Player object, which keeps track of the players match 
history and results. The PlayerList class is used to store and manipulate
a collection of players playing in the same tournament.
"""

__author__ = "Jonas van den Brink"
__email__ = "jonas@simula.no"

import random
import numpy as np
from .match import Match, MatchNotFinished


class PlayerAlreadyDropped(Exception):
    """Thrown when the same player is dropped from a tournament twice."""
    pass

class PlayerAlreadyInTournament(Exception):
    """Thrown when the same player is added to a tournament twice."""
    pass

class PlayerNotInTournament(Exception):
    """Thrown when one attempts to manipulate a player not in the tournament."""
    pass

class PlayerAlreadyMatched(Exception):
    """Thrown when one player is registered to two matches simultaneously."""
    pass

class PlayerNotMatched(Exception):
    """Thrown when a method such as get_opponent is called while player is unmatched."""
    pass

class PlayerMismatch(Exception):
    """Thrown when a result is given that does not match a players actual opponent"""
    pass


class Player(object):
    """Keep track of a player and their stats within a tournament.

    A player is identified by their name, which should there be 
    unique across all players within the same tournament. For each
    player we keep a running list of all their completed matches,
    properties have then been added to compute the current match and 
    game points, as well as opponents match and game win percentages
    from the match history.
    """
    def __init__(self, name):
        """A player is identified by their name, which should be unique

        The player name will be stripped to handle extra whitespace
        and must be at least one character after stripping.

        Parameters
        ----------
        name : str
            The players name
        """
        # Name handling
        self.name = name.strip()
        if len(self.name) < 1:
            raise ValueError("Player names must be at least one character.")
        elif len(self.name) > 25:
            raise ValueError('Player name is too long.')
        elif name == 'BYE':
            raise ValueError('BYE is not a legal name.')

        # Initialize player stats
        self._current_match = None
        self._last_match = None
        self._matches = []
        self._drop_status = -1

    def __str__(self):
        """A Player is represented by their name"""
        return self.name

    # Rich comparisons sort players alphabetically
    def __eq__(self, other):
        if isinstance(other, Player):
            return self.name == other.name
        else:
            return self.name == other 

    def __lt__(self, other):
        if isinstance(other, Player):
            return self.name < other.name 
        else:
            return self.name < other 
    
    def __ne__(self, other):
        return not self == other

    def __le__(self, other):
        return self < other or self == other

    def __ge__(self, other):
        return not self < other

    def __gt__(self, other):
        return not self <= other

    def drop(self):
        """Drop player from tournament."""
        if self._drop_status > -1:
            raise PlayerAlreadyDropped(self.name + "is already dropped.")
        self._drop_status = 1

    def match_with(self, opponent, table=None):
        """Match player with an opponent"""
        if not all((p.current_match is None for p in (self, opponent))):
            raise PlayerAlreadyMatched("Are all results reported?")
        match = Match(self, opponent, table=table)
        self._current_match = match
        if isinstance(opponent, DummyPlayer):
            self.current_match._register_results(2, 0, 0)
        else:
            opponent._current_match = match

    def register_bye(self):
        """Gives the player a Bye.

        Note that the DummyPlayer object is used because this opponent is 
        automatically ignored for calculating OMW/OGW.
        """
        if self.has_had_bye:
            print("Warning: {} has already recieved a Bye".format(self.name))
        self._current_match = Match(self, DummyPlayer())
        self._current_match.register_results(2, 0, 0)
    
    def register_loss(self):
        """Gives the player a Match loss without pairing them against a specific player.

        Note that this function will give the player a match loss against a Dummy Player,
        this is mainly meant to be used if a player is registered for the tournament too late
        and must start with match losses.

        If a player recieves a match loss penalty during a tournament, it should be applied
        directly towards the match in question, or towards that players next match, following 
        normal pairing, meaning it should be registered as a normal match result.
        """
        self._current_match = Match(self, DummyPlayer())
        self._current_match.register_results(0, 2, 0)

    def give_bye(self):
        """Alias for register_bye"""
        self.register_bye()

    def bye(self):
        """Alias for register_bye"""
        self.register_bye()

    def archive_match(self):
        """Used to archive a finished match for scorekeeping.

        The current match is appended to the players list 
        of finished matches, and the current match is set to None.
        """
        if not self.matched:
            raise PlayerNotMatched("No match to archive")
        if not self.current_match.is_completed:
            raise MatchNotFinished("The current match is not finished.")
        self._matches.append(self.current_match)
        self._last_match = self._current_match
        self._current_match = None

    def get_opponent(self, ):
        """Get the currently matched opponent of the player."""
        if not self.matched:
            raise PlayerNotMatched("Player has no opponent")
        return self.current_match.get_opponent(self)

    def register_match(self, opponent, gwins, glosses, gdraws=0):
        """Register a match between the player and a given opponent.

        If neither player is matched, the match is created and registered
        to the player. If the players are matched to each other, the results 
        are registered. If the players are matched, but not to each other,
        raise an error.
        """
        if not self.matched:
            if not opponent.matched:
                self.match_with(opponent)
                self.current_match.register_results(gwins, glosses, gdraws)
            else:
                raise PlayerAlreadyMatched("{} is matched to another "\
                                           "player.".format(opponent))
        else:
            if self.get_opponent() == opponent:
                self.current_match.register_results(gwins, glosses, gdraws)
            else:
                raise PlayerMismatch("{} and/or {} are matched to " \
                                      "other players".format(self, opponent))
    
    def has_met(self, other):
        return other in (m.get_opponent(self) for m in self._matches)
        
    @property    
    def has_had_bye(self):
        return any((isinstance(m.get_opponent(self), DummyPlayer) \
                                         for m in self._matches))

    @property 
    def has_dropped(self):
        return self._drop_status > -1

    @property
    def current_match(self):
        return self._current_match

    @property
    def matched(self):
        return self.current_match is not None

    @property
    def opponents(self):
        return [opp for opp in (m.get_opponent(self) for m in self._matches) \
                                           if not isinstance(opp, DummyPlayer)]

    @property
    def matches_played(self):
        return self._matches

    @property
    def nr_matches(self):
        return len(self._matches)
    
    @property 
    def nr_games(self):
        return sum(m.games_played for m in self._matches)

    @property
    def MP(self):
        """The players number of match points."""
        return sum(m.match_points(self) for m in self._matches)

    @property 
    def MWP(self):
        """The players match win percentage.

        MWP is calculated as number of match points divided by three times 
        the number of rounds the player has played, including byes, or 0.33,
        whichever is higher. Result is given as a number in the range 
        [0.33, 1]. If no matches have been played, nan is returned.
        """
        if self.nr_matches == 0:
            return float('nan')
        return max(0.33, self.MP/(3.0*self.nr_matches))

    @property 
    def GP(self):
        """The players number of game points"""
        return sum(m.game_points(self) for m in self._matches)

    @property 
    def GWP(self):
        """The players game win percentage.

        GWP is calculated as the number of game points divided by three times
        the number of games played. A bye is reported as 2-0-0, and so will 
        add 6 game points and 2 games played. The result is given as 
        a as number in range [0, 1]. If no games have been played, nan 
        is returned.
        """
        if self.nr_games == 0:
            return float('nan')
        return max(0.33, self.GP/(3.0*self.nr_games))

    @property 
    def OMW(self):
        """The players opponent match win.

        OMW is defined as the average of all opponents' MWP.
        Byes are completely ignored for the purposes of OMW. If the 
        player has not played against anyone yet, return nan.
        """
        if len(self.opponents) == 0:
            return float('nan')
        opp_mwp = [opp.MWP for opp in self.opponents if not np.isnan(opp.MWP)]
        return np.sum(opp_mwp)/len(opp_mwp)

    @property
    def OGW(self):
        """The players opponent game win.

        OGW is defined as the average of the all opponents' GWP.
        Byes are completely ignored for the purposes of OGW. If the 
        player has not played against anyone yet, return nan.
        """
        if len(self.opponents) == 0:
            return float('nan')
        opp_gwp = [opp.GWP for opp in self.opponents if not np.isnan(opp.GWP)]
        return np.sum(opp_gwp)/len(opp_gwp)


class DummyPlayer(Player):
    """Class used to create a fake player instance for use in Byes and Match losses.

    When a Player recieves a Bye, it is recorded as a match against 
    a DummyPlayer ending 2-0-0. If a player recives a match loss without being 
    assigned an opponent
    """
    def __init__(self):
        self.name = 'BYE'

    def has_met(self, other):
        return other.has_had_bye

    def archive_match(self):
        pass


class PlayerList(object):
    """Keeps track of all players in a given tournament.
    
    Notes
    -----
    Players should be uniquely defined by their names,
    attempting to add two players with the same name will
    raise an exception.
    """

    def __init__(self):
        """A PlayerList always starts empty."""
        self._players = []

    def __str__(self):
        """Return a list of player names for pretty printing."""
        return "["+", ".join([str(p) for p in self.get_active_players()])+"]"

    def get_player(self, name):
        """Find player object with given name"""
        if not isinstance(name, str):
            raise ValueError("Expected str, got {}".format(type(name)))
        for player in self._players:
            if player.name == name:
                return player
        raise PlayerNotInTournament("Player {} not in tournament.".format(name))

    def get(self, name):
        """Find player object with given name

        Alias of PlayerList.get_player.
        """
        return self.get_player(name)

    def get_players(self, dropped=False):
        """Return a list of all Player objects

        Parameters
        ----------
        dropped : boolean, optional
            Decides if dropped players shold be included in returned list
        """
        if dropped:
            return self.get_all_players()
        else:
            return self.get_active_players()

    def get_active_players(self):
        """Return list of active, i.e., undropped, players"""
        return [p for p in self._players if not p.has_dropped]

    def get_dropped_players(self):
        """Return list of dropped players"""
        return [p for p in self.get_players_by_standing(dropped=True) if p.has_dropped]        

    def get_all_players(self):
        """Return list of all players, including dropped ones."""
        return [p for p in self._players]
    
    def __len__(self):
        return len(self._players)

    def is_enrolled(self, player):
        """Check if given player is enrolled"""
        return any((p == player for p in self._players))
    
    def add(self, player):
        """Adds a new player to the tournament"""
        if self.is_enrolled(player):
            raise ValueError("This player is already enrolled.")
        if isinstance(player, str) or isinstance(player, unicode):
            player = Player(player)
        self._players.append(player)
        self._players.sort()


    def remove(self, name):
        """Remove a given player from Tournament.

        Note that this should only be done before the start of the first 
        round. If a player is dropped from the tournament, they should 
        be marked as dropped.
        """
        try:
            player = self.get(name)
            self._players.remove(player)
        except ValueError:
            raise PlayerNotInTournament

    def get_players_by_name(self, dropped=False):
        """Return a list of players sorted alphabetically
        
        Parameters
        ----------
        dropped : boolean, optional
            Decides if dropped players shold be included
        """
        players = self.get_players(dropped)
        players.sort(key=lambda player: player.name)
        return players

    def get_players_by_standing(self, dropped=False):
        standings = self.get_players(dropped)
        standings.sort(key=lambda player: float('inf') if np.isnan(player.OGW) else player.OGW, reverse=True)
        standings.sort(key=lambda player: float('inf') if np.isnan(player.GWP) else player.GWP, reverse=True)
        standings.sort(key=lambda player: float('inf') if np.isnan(player.OMW) else player.OMW, reverse=True)
        standings.sort(key=lambda player: float('inf') if np.isnan(player.MP) else player.MP, reverse=True)
        return standings        

    def get_current_matches(self):
        """Return all matches of the current round.

        If a player is in a match, that match is added, if a player is
        not in a match, that either means it is before the first round 
        (no matches made yet) or the match result for the round has 
        been reported, in which case the 'current match' is the 
        last match stored in the players history.
        """
        matches = set()
        for p in self.get_active_players():
            if p._current_match is not None:
                matches.add(p._current_match)
            elif p._last_match is not None:
                matches.add(p._last_match)
        matches = sorted(matches, key=lambda x: (x.table is None, x.table))
        return matches

    def get_unreported_matches(self):
        """Return a list of all matches that are awaiting results."""
        return filter(lambda match: match and not match.is_completed,
                      self.get_current_matches())

    def get_completed_matches(self):
        """Return a list of all matches this round that have been reported."""
        return filter(lambda match: match and match.is_completed,
                      self.get_current_matches())
