# -*- encoding: utf-8 -*-

"""
The Registration window is used to add or remove players from
the tournament.
"""


import tkinter as tk
from tkinter import ttk
from .window import Window

class Registration(Window):
    minsize = (900, 860)
    maxsize = (900, 860)
    wm_title = "MTG Event Reporter—Player Registration"
    cframe_padding = (25, 25, 25, 25)
    lbox_size = (25, 24)
    name_width = 24
    wraplength = 380

    def __init__(self, tournament):
        self.tournament = tournament      
        self.setup_main_frame()
        self.setup_content_frame()
        self.setup_listbox_subframe()
        self.setup_button_widgets()
        self.root.mainloop()

    def setup_listbox_subframe(self):
        self.pnames = tk.StringVar()
        self.pnames.set(tuple(self.tournament.get_player_names()))
        self.lboxframe = ttk.Labelframe(self.cframe, text='Players')
        self.lboxframe.grid(column=0, row=0, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.lbox = tk.Listbox(self.lboxframe, width=self.lbox_size[0],
                                               height=self.lbox_size[1], 
                                               listvariable=self.pnames)
        self.scroll = ttk.Scrollbar(self.lboxframe, orient=tk.VERTICAL,
                                                    command=self.lbox.yview)
        self.lbox.configure(yscrollcommand=self.scroll.set)
        self.lbox.grid(column=0, row=0, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.scroll.grid(column=1, row=0, sticky=(tk.N, tk.S, tk.E))
        self.lboxframe.columnconfigure(0, weight=1, minsize=100)
        self.lboxframe.columnconfigure(1, weight=0, minsize=16)
        self.lboxframe.rowconfigure(0, weight=1)
        self.lbox.bind('<Delete>', self.del_player)
        self.player_counter = tk.StringVar()
        self.player_counter.set('Players registered: {}'.format(self.tournament.nr_players()))
        counter = ttk.Label(self.lboxframe, textvariable=self.player_counter,
                            anchor='w')
        counter.grid(column=0, row=1, sticky=(tk.S, tk.W, tk.E))

    def setup_button_widgets(self):
        self.buttonframe = ttk.Frame(self.cframe)
        self.buttonframe.grid(column=2, row=0, padx=40, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.setup_add_player()
        self.setup_del_player()
        self.setup_close_reg()

    def setup_add_player(self):
        widget = ttk.Labelframe(self.buttonframe, text='New Player')
        widget.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E))
        
        self.pname = tk.StringVar()
        self.namebox = ttk.Entry(widget, width=self.name_width+3, textvariable=self.pname)
        self.namebox.grid(column=0, row=1, sticky=(tk.W))
        self.namebox.bind('<Key>', self.validate_text_input_size)
        self.namebox.bind('<Return>', self.add_player)
        
        button = ttk.Button(widget, text=' Add Player ',
                            command=self.add_player, default='active')
        button.grid(column=0, row=2, padx=10, pady=10, sticky=(tk.W))

        self.add_status = tk.StringVar()
        status_label = ttk.Label(widget, textvariable=self.add_status,
                                 anchor='w', wraplength=self.wraplength)
        status_label.grid(column=0, row=3, padx=5, pady=5, sticky=(tk.W, tk.E))
        widget.rowconfigure(index=3, minsize=70)

    def setup_del_player(self):
        widget = ttk.Labelframe(self.buttonframe, text=' Delete Player ')
        widget.grid(column=0, row=1, pady=40, sticky=(tk.N, tk.W, tk.E))

        button = ttk.Button(widget, text='Delete Player',
                            command=self.del_player, default='active')
        button.grid(column=0, row=0, padx=10, pady=10, sticky=(tk.N, tk.W))
        self.del_status = tk.StringVar()
        status_label = ttk.Label(widget, textvariable=self.del_status,
                                 anchor='w', wraplength=self.wraplength)
        status_label.grid(column=0, row=1, pady=5, sticky=(tk.N, tk.W))
        widget.rowconfigure(index=3, minsize=70)

    def setup_close_reg(self):
        button = ttk.Button(self.buttonframe, text=' Close Registration Window ',
                            command=self.close_reg, default='active')
        button.grid(column=0, row=2, sticky=(tk.N, tk.W))
      
    def add_player(self, *args):
        name = self.pname.get()
        try:
            self.tournament.add_player(name)
            players = tuple(self.tournament.get_player_names())
            self.pnames.set(value=players)
            self.player_counter.set('Players registered: {}'.format(len(players)))
            self.namebox.delete(0, 'end')
            self.add_status.set('{} added.'.format(name.strip()))
            self.del_status.set('')
        except ValueError as e:
            self.add_status.set(e)
            self.del_status.set('')
            print('Error: {}'.format(e))

    def del_player(self, *args):
        idxs = self.lbox.curselection()
        if len(idxs) == 0:
            self.add_status.set('')
            self.del_status.set('First select a player to delete.')
        if len(idxs) == 1:
            name = self.tournament.get_player_names()[int(idxs[0])]
            try:
                self.tournament.delete_player(name)
                players = tuple(self.tournament.get_player_names())
                self.pnames.set(value=players)
                self.player_counter.set('Players registered: {}'.format(len(players)))
                self.add_status.set('')
                self.del_status.set('Player {} deleted.'.format(name)) 
            except ValueError as e:
                self.add_status.set('')
                self.del_status.set(e)
                print('Error: {}'.format(e))

    def close_reg(self, *args):
        self.root.destroy()

    def validate_text_input_size(self, event):
        """Check the length of the input when writing"""
        if (event.widget.index(tk.END) >= self.name_width - 1):
            event.widget.delete(self.name_width - 1)
