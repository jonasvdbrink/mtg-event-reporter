from tkinter import ttk
import tkinter as tk
from .window import Window


class Tabs(Window):
    wm_title = "MTG Event Reporter"
    minsize = (1300, 800)
    maxsize = (1300, 800)
    cframe_padding = (25, 25, 25, 25)2
    standings_fmt = '{pos:4} {name:26}{mp:^6}{omw:^5.3} {gwp:^5.3} {ogw:^5.3}'
    standings_height = 24

    def __init__(self):
        self.setup_main_frame()

        nb = ttk.Notebook(self.root)

        standings = ttk.Frame(nb)
        pairings = ttk.Frame(nb)

        nb.add(standings, text='Standings')
        nb.add(pairings, text='Pairings')

        nb.pack(expand=1, fill='both')

        self.root.mainloop() 

#     # adding Frames as pages for the ttk.Notebook 
#     # first page, which would get widgets gridded into it
#     page1 = ttk.Frame(nb)

#     # second page
#     page2 = ttk.Frame(nb)
#     text = ScrolledText(page2)
#     text.pack(expand=1, fill="both")

#     nb.add(page1, text='One')
#     nb.add(page2, text='Two')

#     nb.pack(expand=1, fill="both")

#     root.mainloop()

# if __name__ == "__main__":
#     demo()