import tkinter as tk
from tkinter import ttk
import tkinter.font

class Window(object):
    fontsize = 10

    def __init__(self):
        raise NotImplementedError

    def setup_main_frame(self):
        self.root = tk.Tk()
        self.root.minsize(*self.minsize)
        self.root.maxsize(*self.maxsize)
        self.root.wm_title(self.wm_title)
        self.center_window()

        self.root.protocol('WM_DELETE_WINDOW', self.check_closing)

        default_font = tkinter.font.nametofont("TkDefaultFont")
        default_font.configure(size=self.fontsize)
        self.root.option_add("*Font", default_font)
        self.root.option_add("*Font", "courier 10")


    def setup_content_frame(self):
        self.cframe = ttk.Frame(self.root, padding=self.cframe_padding)
        self.cframe.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E, tk.S))
        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)

    def center_window(self):
        """Center the master frame (root) on the screen.

        FIXME: Works poorly on dual screen setup.
        """
        self.root.update_idletasks()
        w = self.root.winfo_screenwidth()
        h = self.root.winfo_screenheight()
        size = tuple(int(_) for _ in self.root.geometry().split('+')[0].split('x'))
        x = w/2 - size[0]/2
        y = h/2 - size[1]/2
        self.root.geometry("%dx%d+%d+%d" % (size + (x, y)))

    def check_closing(self, *args):
        """Check if user really wants to close the program."""
        self.root.destroy()