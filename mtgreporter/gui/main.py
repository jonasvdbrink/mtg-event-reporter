# -*- encoding: utf-8 -*-

"""
The Main window is shown while the tournament is running. It shows 
the standings and contains buttons to access player registration, 
match results and so forth.
"""

import tkinter as tk
from tkinter import ttk
from .window import Window
from .registration import Registration

class Main(Window):
    wm_title = "MTG Event Reporter"
    minsize = (1300, 800)
    maxsize = (1300, 800)
    cframe_padding = (0, 0, 0, 0)
    standings_fmt = '{pos:4} {name:26}{mp:^6}{omw:^5.3} {gwp:^5.3} {ogw:^5.3}'
    standings_height = 24

    def __init__(self, tournament):
        self.tournament = tournament

        self.setup_main_frame()
        self.setup_content_frame()
        self.setup_button_widgets()
        self.setup_standings_frame()
        self.root.mainloop() 

    def setup_button_widgets(self):
        self.buttonframe = ttk.Frame(self.cframe)
        self.buttonframe.grid(column=0, row=0, padx=25, pady=25, sticky=(tk.N, tk.S, tk.W))
        self.setup_reg_button()

    def setup_reg_button(self):
        regframe = ttk.Labelframe(self.buttonframe, text='Registration')
        regframe.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E))

        regbutton = ttk.Button(regframe, text=' Open Registration ',
                               command=self.open_registration,
                               default='active')
        regbutton.grid(column=0, row=0, padx=10, pady=10, sticky=(tk.N, tk.W))
        regframe.columnconfigure(index=0, minsize=400)

# Box for registration and dropping
#         self.regframe = ttk.Labelframe(self.cframe, text='Registration')
#         self.regbutton = ttk.Button(self.regframe,
#                                text = 'Open Registration',
#                                command = self.open_registration,
#                                default = 'active')
#         self.dropbutton = ttk.Button(self.regframe,
#                                 text = 'Drop Player',
#                                 command = self.drop_player,
#                                 default = 'active')

        # self.roundframe = ttk.Labelframe(self.cframe, text='Round Information')
#         self.roundstatus = StringVar()
#         roundlabel = ttk.Label(self.roundframe, textvariable=self.roundstatus,
#                                anchor="w", font=(PPRINT_FONT, 14))
#         roundlabel.grid(column=0, row=0, sticky=(N, S, W, E))
#         self.roundframe.grid(column=0, row=0, sticky=(N, S, W, E))

#         # Button for printing current standings
#         self.nextround_button = ttk.Button(self.roundframe,
#                                            text = 'Create Next Round',
#                                            command = self.setup_next_round,
#                                            default = 'active')

#         printstandings_button = ttk.Button(self.roundframe, 
#                                            text = 'Print Standings', 
#                                            command = self.print_standings, 
#                                            default = 'active')
#         self.seatings_button = ttk.Button(self.roundframe,
#                                      text = "Print Seatings",
#                                      command = self.print_seatings,
#                                      default = 'active')
#         self.nextround_button.grid(column=0, row=2, sticky=(N, S, W))
#         printstandings_button.grid(column=0, row=3, sticky=(N, S, W))
#         self.seatings_button.grid(column=0, row=4, sticky=(N, S, W))

#         # Box for registration and dropping
#         self.regframe = ttk.Labelframe(self.cframe, text='Registration')
#         self.regbutton = ttk.Button(self.regframe,
#                                text = 'Open Registration',
#                                command = self.open_registration,
#                                default = 'active')
#         self.dropbutton = ttk.Button(self.regframe,
#                                 text = 'Drop Player',
#                                 command = self.drop_player,
#                                 default = 'active')
#         self.dropstatus = StringVar()
#         droplabel = ttk.Label(self.regframe, textvariable=self.dropstatus,
#                                anchor="w", font=(PPRINT_FONT, 11))
#         self.regbutton.grid(column=0, row=0, sticky=(N,S,W,E))
#         self.dropbutton.grid(column=0, row=1, sticky=(N, S, W, E))
#         droplabel.grid(column=0, row=2, columnspan=2, sticky=(N, S, W, E))
#         self.regframe.grid(column=0, row=1, sticky=(N, S, W, E))
#         self.regframe.columnconfigure(0, weight=0, minsize=20)
#         self.regframe.columnconfigure(1, weight=0, minsize=280)


    def setup_standings_frame(self):
        frame = ttk.Labelframe(self.cframe, text='Standings')
        legend = self.standings_fmt.format(pos='Pos.', name='Name', mp='MP', 
                                           omw='OMW', gwp='GWP', ogw='OGW')
        standingslabel = ttk.Label(frame, text=legend, anchor='w')
        
        self.standingsvar = tk.StringVar()
        self.lbox = tk.Listbox(frame, width=len(legend), height=self.standings_height,
                               listvariable=self.standingsvar)
        scrollbar = ttk.Scrollbar(frame, orient=tk.VERTICAL, command=self.lbox.yview)
        self.lbox.configure(yscrollcommand=scrollbar.set)
        self.update_standings()
        # Grid widgets locally
        standingslabel.grid(column=0, row=0, sticky=(tk.N, tk.S, tk.W))
        self.lbox.grid(column=0, row=1, sticky=(tk.N, tk.S, tk.W, tk.E))
        scrollbar.grid(column=1, row=1, sticky=(tk.N, tk.S, tk.W))
        # Grid configure
        # frame.columnconfigure(1, weight=0, minsize=16)
        # frame.columnconfigure(0, weight=0, minsize=650)
        # frame.rowconfigure(0, weight=0, minsize=16)
        # frame.rowconfigure(1, weight=1, minsize=700)
        # Grid frame globally
        frame.grid(column=1, row=0, rowspan=6, padx=25, pady=25, 
                   sticky=(tk.N,tk.S,tk.W,tk.E))

    def open_registration(self):
        self.root.destroy()
        Registration(self.tournament)
        Main(self.tournament)

#     def open_registration(self, *args):
#         """Opens registration window to change players in tournament"""
#         if self.tournament.current_round != 0:
#             e = "Cannot use registration for tournament in progress."
#             raise StandardError(e)
#         self.root.destroy()
#         RegistrationWindow(self.tournament)
#         self.open()



    #     self.setup_listbox_subframe()
    #     self.setup_button_widgets()
    #     self.root.mainloop()
    # def __init__(self, tournament):
    #     self.open()

    # def open(self):
    #     self.root = Tk()
    #     self.standingsvar = StringVar()
        
    #     self.__setup_mainframes()
    #     self.__setup_standings_frame()
    #     self.__setup_buttons()
    #     self.update_standings()
    #     self.update_rounds()
    #     self.update_buttons()
       
    #     self.center_window(self.root)
    #     self.root.mainloop()

#     def __setup_mainframes(self):
#         """Set up root and content frames."""
#         self.cframe.grid(column=0, row=0, sticky=(N,W,E,S))
#         self.root.columnconfigure(0, weight=1)
#         self.root.rowconfigure(0, weight=1)
#         self.cframe.columnconfigure(0, weight=1, minsize=300)
#         self.cframe.columnconfigure(1, weight=0, minsize=600)
#         self.cframe.rowconfigure(0, weight=1, minsize=20)
#         self.cframe.rowconfigure(1, weight=1, minsize=20)
#         self.cframe.rowconfigure(2, weight=1, minsize=20)
#         self.cframe.rowconfigure(3, weight=1, minsize=20)


#     def __setup_buttons(self):
#         """Sets up all buttons used in the main window"""
#         # Box for showing information about current round
#         self.roundframe = ttk.Labelframe(self.cframe, text='Round Information')
#         self.roundstatus = StringVar()
#         roundlabel = ttk.Label(self.roundframe, textvariable=self.roundstatus,
#                                anchor="w", font=(PPRINT_FONT, 14))
#         roundlabel.grid(column=0, row=0, sticky=(N, S, W, E))
#         self.roundframe.grid(column=0, row=0, sticky=(N, S, W, E))

#         # Button for printing current standings
#         self.nextround_button = ttk.Button(self.roundframe,
#                                            text = 'Create Next Round',
#                                            command = self.setup_next_round,
#                                            default = 'active')

#         printstandings_button = ttk.Button(self.roundframe, 
#                                            text = 'Print Standings', 
#                                            command = self.print_standings, 
#                                            default = 'active')
#         self.seatings_button = ttk.Button(self.roundframe,
#                                      text = "Print Seatings",
#                                      command = self.print_seatings,
#                                      default = 'active')
#         self.nextround_button.grid(column=0, row=2, sticky=(N, S, W))
#         printstandings_button.grid(column=0, row=3, sticky=(N, S, W))
#         self.seatings_button.grid(column=0, row=4, sticky=(N, S, W))

#         # Box for registration and dropping
#         self.regframe = ttk.Labelframe(self.cframe, text='Registration')
#         self.regbutton = ttk.Button(self.regframe,
#                                text = 'Open Registration',
#                                command = self.open_registration,
#                                default = 'active')
#         self.dropbutton = ttk.Button(self.regframe,
#                                 text = 'Drop Player',
#                                 command = self.drop_player,
#                                 default = 'active')
#         self.dropstatus = StringVar()
#         droplabel = ttk.Label(self.regframe, textvariable=self.dropstatus,
#                                anchor="w", font=(PPRINT_FONT, 11))
#         self.regbutton.grid(column=0, row=0, sticky=(N,S,W,E))
#         self.dropbutton.grid(column=0, row=1, sticky=(N, S, W, E))
#         droplabel.grid(column=0, row=2, columnspan=2, sticky=(N, S, W, E))
#         self.regframe.grid(column=0, row=1, sticky=(N, S, W, E))
#         self.regframe.columnconfigure(0, weight=0, minsize=20)
#         self.regframe.columnconfigure(1, weight=0, minsize=280)

    def update_standings(self):
        var = []
        standings = self.tournament.get_players_by_standing()
        print('?!')
        print(standings)
        for i, player in enumerate(standings, 1):
            var.append(self.standings_fmt.format(pos=i, name=player.name,
                                                 mp=player.MP, omw=player.OMW,
                                                 gwp=player.GWP, ogw=player.OGW))
        self.standingsvar.set(value=tuple(var))
        for i in range(1, len(var), 2):
            self.lbox.itemconfig(i, {'bg':'light gray'})    
        # listbox.itemconfig(1, {'bg':'red'})

#     def update_standings(self):
#         """Reads standings from Tournament object and updates listbox"""
#         self.standings = self.tournament.get_players_by_standing()
#         formatted = ["%3d. %*s   %2d   %5.3f   %5.3f   %5.3f   %5.3f" \
#                         % (i+1, MAX_LENGTH_PLAYERNAME, p.name, p.MP(),
#                            p.MWP(), p.OMW(), p.GWP(), p.OGW()) \
#                            for i, p in enumerate(self.standings)]
#         self.standingsvar.set(tuple(formatted))

#     def update_rounds(self):
#         """Reads information about which round it is and updates roundstatus"""
#         cr = self.tournament.current_round; mr = self.tournament.max_rounds
#         self.roundstatus.set("Round %d of %d is finished" % (cr, mr))

#     def update_buttons(self):
#         if self.tournament.current_round == 0:
#             self.regbutton.state(['active'])
#             self.dropbutton.state(['disabled'])
#         else:
#             self.regbutton.state(['disabled'])
#             self.dropbutton.state(['active'])

#     def setup_next_round(self, *args):
#         """Creates next round"""
#         if self.tournament.current_round == self.tournament.max_rounds:
#             e = "Max number of rounds reached."
#             raise StandardError(e)
#         self.root.destroy()
#         ResultsWindow(self.tournament)
#         self.open()


#     def drop_player(self, *args):
#         """Drops the currently selected player from the tournament"""
#         idxs = self.lbox.curselection()
#         if len(idxs) == 1:
#             indx = int(idxs[0])
#             player = self.standings[indx].name
#             m = "This will permamently drop player %s from the tournament.\n\n"\
#                     % player
#             m += "This is not reversible.\n\n"
#             m += "Really drop %s?" % player
#             yesno = tkMessageBox.askyesno(message=m, icon='question', 
#                                           title='Drop Player?')
#             if yesno:
#                 self.tournament.drop_player(player)
#                 self.dropstatus.set("Player %s dropped from tournament."%player)
#         else:
#             self.dropstatus.set("Please select a player to drop.")
#         self.update_standings()

#     def check_closing(self, *args):
#         """Check if user really wants to close the program."""
#         self.root.destroy()

#     def print_standings(self, *args):
#         """
#         Write standings to a textfile and open that file
#         so the user can print standings out in paper.
#         """
#         players = self.tournament.get_players_by_standing()
#         filename = "printfiles/standings_round%d.txt" % self.tournament.current_round

#         with open(filename, 'w') as outfile:
#             outfile.write(u"%3s %*s   %2s   %5s   %5s   %5s   %5s\n" % ('Pos.', MAX_LENGTH_PLAYERNAME, 'Name', 'MP', 'MWP', 'OMW', 'GWP', 'OGW'))
#             outfile.write(u"-"*75 + u'\n')
#             for i, p in enumerate(players):
#                 outfile.write(("%3d. %*s   %2d   %5.3f   %5.3f   %5.3f   %5.3f\n" % (i+1, MAX_LENGTH_PLAYERNAME, p.name, p.MP(), p.MWP(), p.OMW(), p.GWP(), p.OGW())).encode('utf8'))
#             outfile.write(u"-"*75 + u"\n")

#         import webbrowser
#         webbrowser.open(filename)

#     def print_seatings(self, *args):
#         """
#         Write seatings (by pod) to a textfile and open that file
#         so the user can print to paper if desired
#         """
#         players = self.tournament.get_players_by_standing()
#         filename = "printfiles/seatings.txt"

#         with open(filename, 'w') as outfile:
#             table = 1
#             try:
#                 assert self.tournament.pods
#                 outfile.write(u"%5s %*s\n" % ('Table', MAX_LENGTH_PLAYERNAME, 'Name'))
                
#                 for i, pod in enumerate(self.tournament.pods):
#                     outfile.write(u"\n"*i + u"-"*10)
#                     outfile.write(u" Pod %s " % string.ascii_lowercase[i])
#                     outfile.write(u"-"*10 + u"\n")
#                     # outfile.write(u"-"*10 + u"Pod %s" % ascii_lowercase[i] + u"-"*10 + u"\n")
#                     for p in pod.get_players():
#                         outfile.write((u"%5s %*s\n" % (table, MAX_LENGTH_PLAYERNAME, p.name)).encode('utf8'))
#                         table += 1
#             except:
#                 for p in self.tournament.get_players():
#                     outfile.write((u"%5s %*s\n" % (table, MAX_LENGTH_PLAYERNAME, p.name)).encode('utf8'))
#                     table += 1

#         import webbrowser
#         webbrowser.open(filename)
        
