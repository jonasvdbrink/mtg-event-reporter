import unittest
import sys
import random
sys.path.append('../src/')

from Player import Player, PlayerAlreadyDropped
from PlayerList import PlayerAlreadyInTournament, PlayerNotInTournament
from Tournament import Tournament
from TournamentStructure import RegistrationNotFinished, \
                                MaxRoundsReached, \
                                PairingsAlreadyCreated, \
                                MissingResults

class TestPlayerMethods(unittest.TestCase):
    def test_add_player(self):
        t = Tournament()
    	t.add_player("Alice")
        self.assertTrue(t._players.is_enrolled("Alice"))
        self.assertRaises(PlayerAlreadyInTournament, t.add_player, "Alice")

    def test_add_players(self):
        t = Tournament()
        t.add_players("Alice", "Bob")
        self.assertTrue(t._players.is_enrolled("Alice"))
        self.assertTrue(t._players.is_enrolled("Bob"))
        self.assertFalse(t._players.is_enrolled("Carol"))
        self.assertRaises(PlayerAlreadyInTournament, t.add_player, "Alice")        
        self.assertRaises(PlayerAlreadyInTournament, 
                          t.add_players, "Carol", "Dylan", "Carol")        

    def test_delete_player(self):
        t = Tournament()
        t.add_players("Alice", "Bob")
        t.delete_player("Alice")
        self.assertTrue(t._players.is_enrolled("Bob"))
        self.assertFalse(t._players.is_enrolled("Alice"))

    def test_drop_player(self):
        t = Tournament()
        t.add_players("Alice", "Bob")
        self.assertFalse(t._players.get("Alice").has_dropped)
        self.assertFalse(t._players.get("Bob").has_dropped)
        t.drop_player("Alice")
        self.assertTrue(t._players.get("Alice").has_dropped)
        self.assertFalse(t._players.get("Bob").has_dropped)
        self.assertRaises(PlayerAlreadyDropped, t.drop_player, "Alice")

    def test_nr_of_players(self):
        t = Tournament()
        t.add_players("Alice", "BoB", "Carol", "Dylan")
        self.assertEqual(t.nr_of_players, 4)

    def test_nr_of_rounds(self):
        t = Tournament()
        self.assertEqual(t.nr_of_rounds, 0)
        t.add_players("Alice", "BoB", "Carol", "Dylan")
        t.finish_registration()
        self.assertEqual(t.nr_of_rounds, 2)

    def test_get_player(self):
        t = Tournament()
        t.add_players("Alice", "Bob", "Carol")
        self.assertEqual(t.get_player("Bob"), Player("Bob"))
        self.assertRaises(PlayerNotInTournament, t.get_player, "Dylan")

    def test_get_active_players(self):
        t = Tournament()
        t.add_players("Alice", "Bob", "Carol", "Dylan")
        t.drop_player("Carol")
        random.seed(0)
        self.assertEqual(t.get_active_players(), [t.get_player("Alice"), 
                         t.get_player("Bob"), t.get_player("Dylan")])
        random.seed(1)
        self.assertEqual(t.get_active_players(), [t.get_player("Dylan"), 
                         t.get_player("Bob"), t.get_player("Alice")])

    def test_get_acive_players(self):
        t = Tournament()
        t.add_players("Bob", "Dylan", "Alice", "Carol")
        t.drop_player("Carol")
        self.assertEqual(t.get_players_by_name(), [t.get_player("Alice"), 
                         t.get_player("Bob"), t.get_player("Dylan")])

    def test_get_players_by_standing(self):
        pass

    def test_get_current_round(self):
        t = Tournament()
        t.add_players("Alice", "Bob", "Carol", "Dylan")
        t.finish_registration()
        self.assertEqual(t.get_current_round(), 0)
        t.create_next_round()
        self.assertEqual(t.get_current_round(), 1)
    
    def test_create_next_round(self):
        t = Tournament()
        random.seed(0)
        t.add_players("Alice", "Bob", "Carol", "Dylan")
        self.assertRaises(RegistrationNotFinished, t.create_next_round)
        t.finish_registration()
        t.create_next_round()
        self.assertRaises(MissingResults, t.create_next_round)
        matches = [str(m) for m in t.get_matches()]
        self.assertEqual(", ".join(matches), 'Alice vs. Dylan 0-0-0, ' \
                                             'Bob vs. Carol 0-0-0')
        self.assertRaises(MissingResults, t.create_next_round)                
        for m in t.get_matches():
            m.update_match_status(m._player_one, 2, 1)
        t.create_next_round()
        for m in t.get_matches():
            m.update_match_status(m._player_one, 2, 1)
        self.assertRaises(MaxRoundsReached, t.create_next_round)

    def test_register_match(self):
        t = Tournament()
        random.seed(0)
        t.add_players("Alice", "Bob", "Carol", "Dylan")
        t.finish_registration()
        t.create_next_round()
        self.assertEqual(len(t.get_unreported_matches()), 2)
        t.register_match("Alice", "Dylan", 2, 0, 0)       
        self.assertEqual(len(t.get_unreported_matches()), 1)
        t.register_match("Bob", "Carol", 1, 2, 0)
        self.assertEqual(len(t.get_unreported_matches()), 0)
        standings = [str(p) for p in t.get_players_by_standing()]
        self.assertEqual(standings, ['Alice', 'Carol', 'Bob', 'Dylan'])

    def test_has_met(self):
        t = Tournament()
        random.seed(0)
        t.add_players("Alice", "Bob", "Carol", "Dylan")
        t.finish_registration()
        t.create_next_round()
        self.assertFalse(t.has_met("Alice", "Dylan"))
        self.assertFalse(t.has_met("Bob", "Carol"))
        self.assertFalse(t.has_met("Bob", "Dylan"))
        t.register_match("Alice", "Dylan", 2, 0, 0)       
        self.assertTrue(t.has_met("Alice", "Dylan"))
        self.assertFalse(t.has_met("Bob", "Carol"))
        self.assertFalse(t.has_met("Bob", "Dylan"))
        t.register_match("Bob", "Carol", 1, 2, 0)
        self.assertTrue(t.has_met("Alice", "Dylan"))
        self.assertTrue(t.has_met("Bob", "Carol"))
        self.assertFalse(t.has_met("Bob", "Dylan"))
        t.create_next_round()
        self.assertTrue(t.has_met("Alice", "Dylan"))
        self.assertTrue(t.has_met("Bob", "Carol"))
        self.assertFalse(t.has_met("Bob", "Dylan"))

    def test_get_random_match(self):
        t = Tournament()
        random.seed(0)
        t.add_players("Alice", "Bob", "Carol", "Dylan")
        t.finish_registration()
        t.create_next_round()
        random.seed(1)
        self.assertEqual(str(t.get_random_match()), 'Alice vs. Dylan 0-0-0')
        random.seed(2)
        self.assertEqual(str(t.get_random_match()), 'Bob vs. Carol 0-0-0')

if __name__ == '__main__':
	unittest.main()

