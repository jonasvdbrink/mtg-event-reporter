import unittest
import sys
sys.path.append('../src/')

from Player import Player
from Match import Match, PlayerNotInMatch, InvalidMatchResult


class TestMatch(unittest.TestCase):
    def setUp(self):
        self.p1 = Player('Alice')
        self.p2 = Player('Bob')
        self.p3 = Player('Carol')

    def test_init(self):
        m = Match(self.p1, self.p2)
        self.assertEqual(m._player_one, self.p1)
        self.assertEqual(m._player_two, self.p2)
        self.assertFalse(m.is_completed)
        self.assertRaises(PlayerNotInMatch, m.update_match_status, self.p3, 2, 0)

    def test___repr___(self):
        m = Match(self.p1, self.p2)
        self.assertEqual(m.__repr__(), "Match(Alice, Bob, 0, 0, 0)")
        m.update_match_status(self.p1, 2, 1, 1)
        self.assertEqual(m.__repr__(), "Match(Alice, Bob, 2, 1, 1)")

    def test_update_match_status(self):
        m = Match(self.p1, self.p2)
        self.assertRaises(InvalidMatchResult, m.update_match_status, self.p1, -1, 0)
        self.assertRaises(InvalidMatchResult, m.update_match_status, self.p1, 0, -1)
        self.assertRaises(InvalidMatchResult, m.update_match_status, self.p1, 0, 0, -1)

        m.update_match_status(self.p1, 2, 0)
        self.assertEqual(m._games_won_player_one, 2)
        self.assertEqual(m._games_won_player_two, 0)
        self.assertEqual(m._games_drawn, 0)
        self.assertTrue(m.is_completed)

        m.update_match_status(self.p2, 2, 0, 1)
        self.assertEqual(m._games_won_player_one, 0)
        self.assertEqual(m._games_won_player_two, 2)
        self.assertEqual(m._games_drawn, 1)

        m.update_match_status(self.p2, 1, 1, 1)
        self.assertEqual(m._games_won_player_one, 1)
        self.assertEqual(m._games_won_player_two, 1)
        self.assertEqual(m._games_drawn, 1)

        m.update_match_status(self.p1, 0, 1, 1)
        self.assertEqual(m._games_won_player_one, 0)
        self.assertEqual(m._games_won_player_two, 1)
        self.assertEqual(m._games_drawn, 1)

    def test_get_opponent(self):
        f = Match(self.p1, self.p2).get_opponent
        self.assertEqual(f(self.p1), self.p2)
        self.assertEqual(f(self.p2), self.p1)
        self.assertRaises(PlayerNotInMatch, f, self.p3)

    def test_games_played(self):
        m = Match(self.p1, self.p2)
        m.update_match_status(self.p1, 2, 0)
        self.assertEqual(m.games_played(), 2)

        m.update_match_status(self.p2, 2, 0)
        self.assertEqual(m.games_played(), 2)

        m.update_match_status(self.p1, 0, 2)
        self.assertEqual(m.games_played(), 2)

        m.update_match_status(self.p1, 2, 1)
        self.assertEqual(m.games_played(), 3)

        m.update_match_status(self.p1, 1, 1, 1)
        self.assertEqual(m.games_played(), 3)

        m.update_match_status(self.p1, 1, 0, 1)
        self.assertEqual(m.games_played(), 2)

        m.update_match_status(self.p1, 2, 1, 1)
        self.assertEqual(m.games_played(), 4)

    def test_games_won(self):
        m = Match(self.p1, self.p2)
        self.assertRaises(PlayerNotInMatch, m.games_won, self.p3)

        m.update_match_status(self.p1, 2, 0)
        self.assertEqual(m.games_won(self.p1), 2)

        m.update_match_status(self.p1, 0, 2)
        self.assertEqual(m.games_won(self.p1), 0)

        m.update_match_status(self.p1, 2, 1)
        self.assertEqual(m.games_won(self.p1), 2)

        m.update_match_status(self.p1, 1, 1, 1)
        self.assertEqual(m.games_won(self.p1), 1)

        m.update_match_status(self.p1, 1, 0, 1)
        self.assertEqual(m.games_won(self.p1), 1)

        m.update_match_status(self.p1, 0, 1, 1)
        self.assertEqual(m.games_won(self.p1), 0)

        m.update_match_status(self.p1, 2, 1, 1)
        self.assertEqual(m.games_won(self.p1), 2)

    def test_get_match_points(self):
        m = Match(self.p1, self.p2)
        m.update_match_status(self.p1, 2, 1)

        self.assertRaises(PlayerNotInMatch, m.get_match_points, self.p3)

        self.assertEqual(m.get_match_points(self.p1), 3)
        self.assertEqual(m.get_match_points(self.p2), 0)

        m.update_match_status(self.p1, 1, 1, 1)
        self.assertEqual(m.get_match_points(self.p1), 1)
        self.assertEqual(m.get_match_points(self.p2), 1)

        m.update_match_status(self.p1, 0, 0, 3)
        self.assertEqual(m.get_match_points(self.p1), 1)
        self.assertEqual(m.get_match_points(self.p2), 1)


if __name__ == '__main__':
    unittest.main()
