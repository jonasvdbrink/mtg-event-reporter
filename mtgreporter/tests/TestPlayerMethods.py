import unittest
import sys
sys.path.append('../src/')

from Player import Player, ByePlayer, PlayerNotMatched


class TestPlayerMethods(unittest.TestCase):

    def test_drop(self):
        p1 = Player('Alice')
        p1.drop(0)
        self.assertTrue(p1.has_dropped)

    def test_undrop(self):
        p1 = Player('Alice')
        p1.drop(0)
        p1.undrop(0)
        self.assertFalse(p1.has_dropped)

    def test_match_with(self):
        p1 = Player('Alice')
        p2 = Player('Bob')
        self.assertFalse(p1.matched)
        self.assertFalse(p2.matched)
        p1.match_with(p2)
        self.assertTrue(p1.matched)
        self.assertTrue(p2.matched)

    def test_get_current_opponent(self):
        p1 = Player('Alice')
        p2 = Player('Bob')
        p1.match_with(p2)
        self.assertEqual(p1.get_current_opponent(), p2)

    def test_register_match(self):
        p1 = Player('Alice')
        p2 = Player('Bob')
        p3 = Player('Carol')
        self.assertRaises(PlayerNotMatched, p1.register_match, p2, 2, 0)
        p1.match_with(p2)
        p3.match_with(ByePlayer())
        p1.register_match(p2, 2, 1)
        self.assertEqual(p1.MP(), 3)
        self.assertEqual(p2.MP(), 0)
        self.assertEqual(p3.MP(), 3)
        self.assertEqual(p1.MWP(), 1.0)
        self.assertEqual(p2.MWP(), 0.33)
        self.assertEqual(p3.MWP(), 1)
        self.assertEqual(p1.GWP(), 2./3)
        self.assertEqual(p2.GWP(), 1./3)
        self.assertEqual(p3.GWP(), 1)
        self.assertEqual(p1.OMW(), 0.33)
        self.assertEqual(p2.OMW(), 1.00)
        self.assertEqual(p3.OMW(), 0.33)
        self.assertEqual(p1.OGW(), 1./3)
        self.assertEqual(p2.OGW(), 2./3)
        self.assertEqual(p3.OGW(), 0.33)
        p1.match_with(p3)
        p1.register_match(p3, 1, 2)
        p2.match_with(ByePlayer())
        self.assertEqual(p1.MP(), 3)
        self.assertEqual(p2.MP(), 3)
        self.assertEqual(p3.MP(), 6)
        self.assertEqual(p1.MWP(), 1./2)
        self.assertEqual(p2.MWP(), 1./2)
        self.assertEqual(p3.MWP(), 1)
        self.assertEqual(p1.GWP(), 1./2)
        self.assertEqual(p2.GWP(), 3./5)
        self.assertEqual(p3.GWP(), 4./5)
        self.assertEqual(p1.OMW(), 3./4)
        self.assertEqual(p2.OMW(), 2./4)
        self.assertEqual(p3.OMW(), 2./4)

    def test_has_met(self):
        p1 = Player('Alice')
        p2 = Player('Bob')
        p3 = Player('Carol')
        self.assertFalse(p1.has_met(p2))
        self.assertFalse(p1.has_met(p3))
        self.assertFalse(p2.has_met(p1))
        self.assertFalse(p2.has_met(p3))
        self.assertFalse(p3.has_met(p1))
        self.assertFalse(p3.has_met(p2))
        p1.match_with(p2)
        p1.register_match(p2, 2, 0)
        self.assertTrue(p1.has_met(p2))
        self.assertTrue(p2.has_met(p1))
        self.assertFalse(p1.has_met(p3))
        self.assertFalse(p3.has_met(p1))
        self.assertFalse(p2.has_met(p3))
        self.assertFalse(p3.has_met(p2))


if __name__ == '__main__':
    unittest.main()
