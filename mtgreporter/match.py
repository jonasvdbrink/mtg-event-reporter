# -*- encoding: utf-8 -*-

"""
Every match in a tournament is stored as a match object in memory,
each players has their own personal history linking to these match
objects, enabling calculating tiebreakers and finding pairings.
"""

class InvalidMatchResult(Exception):
    """The entered match result is not valid.

    A result where a player won more than two games is 
    an invalid result. 
    A 0-0-0 is also an invalid result, as a intentional 
    draw should be marked as a 0-0-3 (a 0-0-1 is accepted
    according to MTR, but is abnormal).
    """
    pass

class MatchNotFinished(Exception):
    """Thrown when one tries to archive a match that is not finished."""
    pass


class Match(object):
    """Represent a single match between two players..

    A match consists of two players playing a number of games. A match
    concludes when either player reaches two won games or the round-time
    runs out, in which the current game is marked as a draw.
    Matches are _not_ 'best-of-3', as there can be an arbitrary number of 
    drawn games. Note that an intential drawn match shal be reported as 
    0-0-3 in most tournaments, though the MTR states that a 0-0-1 may also
    be used if desired.
    """
    def __init__(self, player_one, player_two, table=None):
        """A match is defined by the two players meeting.

        When a match object is created, it starts with no results
        registered and is flagged as incompleted. When results are 
        registered the flag is changed.

        Parameters
        ----------
        player_one : Player
            The first player of the match
        player_two : Player
            The second player of the match
        table : int, optional
            Table number for match
        """
        self.player_one = player_one 
        self.player_two = player_two
        self.games_won_player_one = 0
        self.games_won_player_two = 0
        self.games_drawn = 0
        self.is_completed = False
        self.table = table

    def __str__(self):
        """Pretty printing of Matches"""
        s = u'Table {}: '.format(self.table or '-')
        s += u'{p1_name} ({p1_mp}) vs. {p2_name}'
        if self.player_two.name != 'BYE':
            s += '({p2_mp})'.format(p2_mp=self.player_two.MP)
        if self.is_completed:
            s += u' ({gw}-{gl}-{gd})'    
        return s.format(p1_name=self.player_one.name, 
                        p2_name=self.player_two.name, 
                        p1_mp=self.player_one.MP,
                        gw = self.games_won_player_one,
                        gl = self.games_won_player_two,
                        gd = self.games_drawn)
    

    def register_results(self, gw, gl, gd=0, reverse=False, completed=True):
        """Register the results of the match.

        Note that the results given are relative to the order of the players
        in the match.

        Parameters
        ----------
        gw : int
            Games one by player one
        gl : int
            Games lost by player one
        gd : int, optional
            Games drawn, defaults to 0
        reverse : bool, optional
            Results are registered as if the players were swapped
        completed : bool, optional
            In addition to altering the game results, mark match as complete
        """
        if not all((isinstance(v, int) for v in (gw, gl, gd))):
            raise InvalidMatchResult("Match result must be integer")
        if not all((v >= 0 for v in (gw, gl, gd))):
            raise InvalidMatchResult("Results must be non-negative")
        if gw > 2:
            raise InvalidMatchResult("More than two games won")
        if gw + gl + gd == 0:
            raise InvalidMatchResult("No games played")

        # Register results
        if not reverse:
            self.games_won_player_one = gw 
            self.games_won_player_two = gl 
        else:
            self.games_won_player_two = gw 
            self.games_won_player_one = gl         
        self.games_drawn = gd 

        # Archive results if match is finished
        if completed:
            self.is_completed = completed 
            self.player_one.archive_match()
            self.player_two.archive_match()
    
    def update_results(self, gw, gl, gd=0, reverse=False, completed=True):
        """Alias for register_results method."""
        return self.register_results()

    def register(self, gw, gl, gd=0, reverse=False, completed=True):
        """Alias for register_results method."""
        return self.register_results()

    def update(self, gw, gl, gd=0, reverse=False, completed=True):
        """Alias for register_results method."""
        return self.register_results()

    def get_opponent(self, player):
        """Given one player in the match, return the other"""
        if player == self.player_one:
            return self.player_two
        elif player == self.player_two:
            return self.player_one
        raise PlayerNotInMatch

    @property
    def games_played(self):
        """Return number of games played in the match"""
        return (self.games_won_player_one 
                + self.games_won_player_two 
                + self.games_drawn)

    def game_points(self, player):
        """Number of game points the match is worth to the given player"""
        if player == self.player_one:
            return 3*self.games_won_player_one + self.games_drawn
        if player == self.player_two:
            return 3*self.games_won_player_two + self.games_drawn 
        else:
            raise PlayerNotInMatch

    def match_points(self, player):
        """Number of match points the match is worth to the given player"""
        p1 = self.games_won_player_one
        p2 = self.games_won_player_two 

        if player is self.player_one:
            return 3*(p1 > p2) + 1*(p1 == p2)
        elif player is self.player_two:
            return 3*(p2 > p1) + 1*(p1 == p2)
        else:
            raise PlayerNotInMatch