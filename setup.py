import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "MTG Event Reporter",
    version = "0.1",
    author = "Jonas van den Brink",
    author_email = "jonasvdbrink@gmail.com",
    description = ("A simple tool to create pairings and generate tie breakers"
                      "for magic tournaments."),
    packages = ["mtgreporter"],
    url = "https://bitbucket.org/jonasvdbrink/mtg-event-reporter",
    long_description=read('README.md'))